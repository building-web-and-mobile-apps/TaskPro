# Weekly Report - Week 13

Sahan Wijesinghe
- Updated Project Progress worksheet with current status of each specification
- Created a breakdown of task allocation to each team member by category

Thomas Niestroj
- Creating Screens for 2FA (add number, verify phone number)
- User roles and permission (create admin page, adjust backend, check userrole and change frontend accordingly)

Mohammed Amine Malloul
- Deploy to azure vs using Docker Compose
- DockerFile: 🐛 Install the Dart GPG key

Jonas Wagner
- Frontend for File Handling uploading/downloading/deleting file attachments to tasks
- Error handling for import Frontend
- Backend Unit Testing for all major packages
- Frontend Documentation
