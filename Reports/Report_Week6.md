# Weekly Report - Week 6

Sahan Wijesinghe
- Refactored /user endpoints to be consistent with /task endpoints

Thomas Niestroj
- setting up toking verification in backend
- design adjustments mainpage

Mohammed Amine Malloul
- Add Dockerfile for Flutter web application (Web Build Only) 
- Add Dockerfile for Spring Boot application 

Jonas Wagner
- Frontend integration for Sort, Filter & Search API endpoints
