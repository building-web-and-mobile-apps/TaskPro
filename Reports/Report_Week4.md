# Weekly Report - Week 4

Sahan Wijesinghe
- Implemented audit service which uses a logging library to log and track user activities for auditing purposes
- Implemented calling an external API in the backend (to fetch the weather of a given city)

Thomas Niestroj
- Progess in connecting Firebase Auth with our Spring Boot Backend (JWT Token verification)
- minor Frontend adjustments

Mohammed Amine Malloul
- Implemeted Task Searching, Filtering and Sorting functionalities 
- Implemeted Task Data seeder

Jonas Wagner
- Updating task list and in general the frontend on any changes for example adding a new task, updating, deleteing, etc.
- Help out with logging problem in Backend
- Add tests for Flutter Frontend including simple tests and API call tests
