# Weekly Report - Week 5

Sahan Wijesinghe
- Drafted an initial version of documentation for the backend

Thomas Niestroj
- start documentation of Frontend
- Connecting Firebase Auth with Spring Boot Backend

Mohammed Amine Malloul
- Fixed handling of null and empty parameters in Task filter endpoint 

Jonas Wagner
- Draft for documentation for Frontend and Mobile Application
- Add Weather API Backend to Frontend
