# Weekly Report - Week 12

Sahan Wijesinghe
- Updated documentation of backend project structure and packages

Thomas Niestroj
- minor Frontend improvements
- adjusting Backend for 2FA and preparing it for User roles

Mohammed Amine Malloul
- Fix failing CI pipline

Jonas Wagner
- Testing for the whole Backend
