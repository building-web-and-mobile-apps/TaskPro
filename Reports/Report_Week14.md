# Weekly Report - Week 14

Sahan Wijesinghe
- Added documentation with detailed instructions elucidating the procedure for executing the project on the Linux Virtual Machine (VM)

Thomas Niestroj
- fix leftover tasks before final deployment
- various minor bugfixes
- preparing branches for showing progress on 2FA

Mohammed Amine Malloul
- Implement https using certbot and nginx
- (Final) deployment

Jonas Wagner
- Check loggin in major Backend Controllers
- Improve Documentation in Frontend + Backend
- (Final) deployment
