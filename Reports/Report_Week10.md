# Weekly Report - Week 10

Sahan Wijesinghe
- Created backend endpoints for Data Export/Import

Thomas Niestroj
- Code refactoring
- Documentation

Mohammed Amine Malloul
- feat: ✨ add capability to upload multiple files to a specific task
- feat: ✨ implement retrieval of file attachments for a specific task

Jonas Wagner
- Own Table for Category (Backend + Frontend)
- Own Table for Priorities (Backend + Frontend)
- User specific tasks (Backend + Frontend)
