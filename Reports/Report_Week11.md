# Weekly Report - Week 11

Sahan Wijesinghe
- Added admin endpoints to manage users

Thomas Niestroj
- starting implementation of 2FA

Mohammed Amine Malloul
- Refactor File Attachment functions to FileAttachment Service
- feat: implement attachement of a task deletion functionality

Jonas Wagner
- Frontend Integration for Import + Export
