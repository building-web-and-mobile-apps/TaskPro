# Weekly Report - Week 8

Sahan Wijesinghe
- Added skeleton for Audit aspect (auditing user activities)

Thomas Niestroj
- Firebase_auth_ui bugfix
- securing backend with firebase token validation
- adding logout button to appbar

Mohammed Amine Malloul
- ci: ⚡ Add CI Pipeline to automate deployment 

Jonas Wagner
- Flyway Database Implementation in Backend
