# Weekly Report - Week 1

## Summary
- Prepare Project
- First Commit
- Assign Tasks

## Tasks Completed
- Decide on tech stack for development
- Categorised requirements into priority levels
- Assigned requirements to team members
- Decided on timeline for development
- Create GitLab repository
- Prepare presentation
- First Commit for Frontend
- Report Template

## Next Steps
- Start development
- Create a basic frontend
- Create a basic backend
- Create a basic database
