# Weekly Report - Week 2

Sahan Wijesinghe
- Created basic structure for crud operations of TaskItem entity
- Configured Spring Security for authentication

Thomas Niestroj
- Sign-In and Registration Pages
- Start integration of Firebase Auth

Mohammed Amine Malloul
- refactor: Restructured project, added dependencies, resolved warnings, and integrated with MySQL for development
- Started the application to ensure successful integration.

Jonas Wagner
- Core Screens for ToDo Application
- Adjusted Screens to fit our Task Definition (all fields)
- Fix responsiveness
- Adjust color of form buttons