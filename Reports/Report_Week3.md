# Weekly Report - Week 3

Sahan Wijesinghe
- Refactored task controller endpoints and added response entities

Thomas Niestroj
- Resolving issues after integration Firebase Auth
- Design changes in Sign-In Frontend

Mohammed Amine Malloul
- Implemented User Module with model, repository, service, and controller for user management 

Jonas Wagner
- Prepare API calls in Frontend
- Implement GET all, GET single, POST and DELETE endpoints in Frontend
- Prepare PUT and status endpoints
- Cors Fix for Chrome with Flutter
- Implement update task and status endpoints
