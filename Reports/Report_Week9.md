# Weekly Report - Week 9

Sahan Wijesinghe
- Logged user activities by Firebase User ID
- Refactored Task Controller

Thomas Niestroj
- continue on Frontend documentation
- fixing minor bugs

Mohammed Amine Malloul
- Start Working on file managements in Backend.

Jonas Wagner
- Integratie Auth Token to API calls in Frontend + Fixes
- Chat Integration with Firebase
- Start working on user specific Tasks
