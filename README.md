# TaskPro Task Management Application
Graded Project for Building Web and Mobile Apps WiSe 23/24

# Documentation

- [Frontend Documentation](Frontend/README.md)
- [Backend Documentation](Backend/README.md)
- [Documentation to set up and run the application on a virtual machine](vm-setup.md)

# Team Members
- Sahan Wijesinghe
- Thomas Niestroj
- Mohammed Amine Malloul
- Jonas Wagner

# Final Versions (Docker Images & Tags)

## Local VM
- aminemall/bwma-todo-frontend:1.2710
- aminemall/bwma-todo-backend:1.2710

## Azure VM (using HTTPS setup)
- aminemall/bwma-todo-frontend:1.2707
- aminemall/bwma-todo-backend:1.2707

# Project Progress 
- https://docs.google.com/spreadsheets/d/1KACQRlGLKGE-Qqkoq-gdFeSl-G-ftFIu/edit#gid=348936345