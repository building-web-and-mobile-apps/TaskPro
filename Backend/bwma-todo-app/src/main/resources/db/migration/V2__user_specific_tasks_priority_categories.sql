ALTER TABLE tasks
ADD COLUMN userid BIGINT;

CREATE TABLE categories (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);

INSERT INTO categories (name) VALUES ('Work');
INSERT INTO categories (name) VALUES ('Personal');
INSERT INTO categories (name) VALUES ('Other');

CREATE TABLE priorities (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);

INSERT INTO priorities (name) VALUES ('Low');
INSERT INTO priorities (name) VALUES ('Medium');
INSERT INTO priorities (name) VALUES ('High');