ALTER TABLE audit MODIFY parameters TEXT;

INSERT INTO users (email, name, created_at, updated_at)
VALUES ('example@example.com', 'John Doe', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
