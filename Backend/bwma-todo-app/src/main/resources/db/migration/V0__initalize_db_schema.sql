CREATE TABLE users (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(255),
    name VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE tasks (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    description TEXT,
    done BOOLEAN,
    due_date DATETIME,
    priority VARCHAR(255),
    category VARCHAR(255),
    complete_date DATETIME,
    archived BOOLEAN
);

CREATE TABLE audit (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(255),
    uri VARCHAR(255),
    method VARCHAR(255),
    parameters TEXT,
    start_time DATETIME,
    user VARCHAR(255)
);
