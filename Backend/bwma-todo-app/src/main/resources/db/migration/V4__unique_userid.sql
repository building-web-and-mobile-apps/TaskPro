ALTER TABLE users
ADD CONSTRAINT unique_userid UNIQUE (userid);

UPDATE users SET userid = UUID();