package de.hsfulda.bwmatodoapp.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserModel, Long> {
    @Query(value = "SELECT * FROM users WHERE userid = ?1", nativeQuery = true)
    Optional<UserModel> findByUserId(String userId);
}
