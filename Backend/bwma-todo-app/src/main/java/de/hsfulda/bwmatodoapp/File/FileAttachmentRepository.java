package de.hsfulda.bwmatodoapp.File;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FileAttachmentRepository extends JpaRepository<FileAttachment, Long> {

    List<FileAttachment> findByTaskId(Long taskId);
}
