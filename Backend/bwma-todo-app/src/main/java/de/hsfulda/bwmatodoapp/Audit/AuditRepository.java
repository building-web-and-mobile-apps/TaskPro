package de.hsfulda.bwmatodoapp.Audit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AuditRepository extends JpaRepository<AuditItem, Long>, JpaSpecificationExecutor<AuditItem> {
}
