package de.hsfulda.bwmatodoapp.auth;

import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service("authService")
public class AuthService {
    Authentication authentication;
    public boolean isAdmin() {
        try {
            this.authentication = SecurityContextHolder.getContext().getAuthentication();
            String uid = (String) authentication.getPrincipal();
            Firestore db = FirestoreClient.getFirestore();
            DocumentSnapshot document = db.collection("users").document(uid).get().get();
            return document.getBoolean("admin");
        } catch (Exception e) {
            return false;
        }
    }
}
