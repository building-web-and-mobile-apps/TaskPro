package de.hsfulda.bwmatodoapp.Task;

import de.hsfulda.bwmatodoapp.Audit.AuditConstants;
import de.hsfulda.bwmatodoapp.File.FileAttachment;
import de.hsfulda.bwmatodoapp.File.FileAttachmentService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path = "/tasks")
@Slf4j
public class TaskController {


    private final TaskService taskService;
    private final FileAttachmentService fileAttachmentService;

    public TaskController(TaskService taskService,
                          FileAttachmentService fileAttachmentService){
        this.taskService = taskService;
        this.fileAttachmentService = fileAttachmentService;

    }

    @GetMapping
    public ResponseEntity<?> getTasks(){
        try {
            log.info("Retrieving all tasks");
            List<TaskItem> tasks = taskService.getTasks();
            return new ResponseEntity<>(tasks, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Failed to retrieve all tasks");
            return new ResponseEntity<>(AuditConstants.GET_TASKS_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<?> addTask(@Valid @RequestBody TaskItem taskItem){
        try {
            log.info("Trying to add task: {} ", taskItem);
            TaskItem task = taskService.addTask(taskItem);
            if (task == null) {
                log.error("400 Failed to add task: {} ", taskItem);
                return new ResponseEntity<>(AuditConstants.ADD_TASK_FAILED, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(task, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("500 Failed to add task: {} ", taskItem);
            return new ResponseEntity<>(AuditConstants.ADD_TASK_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/bulk")
    public ResponseEntity<?> addTasks(@Valid @RequestBody TaskItems taskItems){
        try {
            log.info("Trying to add multiple tasks");
            List<TaskItem> tasksAdded = taskService.addTasks(taskItems.getTaskItems());
            if (tasksAdded.isEmpty()){
                log.error("400 Failed to add multiple tasks");
                return new ResponseEntity<>(AuditConstants.ADD_TASKS_FAILED, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(tasksAdded, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("500 Failed to add multiple tasks");
            return new ResponseEntity<>(AuditConstants.ADD_TASKS_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/status/{id}")
    public ResponseEntity<?> updateTask(@PathVariable Long id){
        try {
            log.info("Trying to update task status: {} ", id);
            TaskItem updatedTask = taskService.updateTask(id);
            if (updatedTask == null) {
                log.error("400 Failed to update task status: {} ", id);
                return new ResponseEntity<>(AuditConstants.UPDATE_TASK_FAILED, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(updatedTask, HttpStatus.OK);
        } catch (Exception e) {
            log.error("500 Failed to update task status: {} ", id);
            return new ResponseEntity<>(AuditConstants.UPDATE_TASK_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateTask(@PathVariable Long id, @Valid @RequestBody TaskItem taskItem){
        try{
            log.info("Trying to update task: {} ", id);
            TaskItem updatedTask = taskService.updateTask(id, taskItem);
            if (updatedTask == null) {
                log.error("400 Failed to update task: {} ", id);
                return new ResponseEntity<>(AuditConstants.UPDATE_TASK_FAILED, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(updatedTask, HttpStatus.OK);
        }
        catch (Exception e) {
            log.error("500 Failed to update task: {} ", id);
            return new ResponseEntity<>(AuditConstants.UPDATE_TASK_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable Long id){
        try{
            log.info("Trying to delete task: {} ", id);
            TaskItem task = taskService.getTask(id);
            if (task != null) {
                taskService.deleteTask(id);
                return new ResponseEntity<>(AuditConstants.DELETE_TASK + id, HttpStatus.OK);
            }
            log.error("404 failed to delete task. Task not found with id: {} ", id);
            return new ResponseEntity<>(AuditConstants.NO_TASK_FOUND, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            log.error("500 Failed to delete task: {} ", id);
            return new ResponseEntity<>(AuditConstants.DELETE_TASK_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getTask(@PathVariable Long id){
        try{
            log.info("Trying to retrieve task: {} ", id);
            TaskItem task = taskService.getTask(id);
            if (task != null) {
                return new ResponseEntity<>(task, HttpStatus.OK);
            }
            log.error("404 failed to retrieve task. Task not found with id: {} ", id);
            return new ResponseEntity<>(AuditConstants.NO_TASK_FOUND, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e) {
            log.error("500 Failed to retrieve task: {} ", id);
            return new ResponseEntity<>(AuditConstants.GET_TASK_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    GET /api/tasks/search?keyword={keyword}
    @GetMapping("/search")
    public ResponseEntity<?> searchTasks(@RequestParam String keyword, @RequestParam String userId){
        try{
            log.info("Trying to search tasks with keyword: {} ", keyword);
            List<TaskItem> tasks = taskService.searchTasks(keyword, userId);
            if (tasks.isEmpty()) {
                log.error("404 failed to search tasks. No tasks found with keyword: {} ", keyword);
                return new ResponseEntity<>(AuditConstants.NO_TASKS_FOUND, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(tasks, HttpStatus.OK);
        }
        catch (Exception e) {
            log.error("500 Failed to search tasks with keyword: {} ", keyword);
            return new ResponseEntity<>(AuditConstants.SEARCH_TASKS_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    GET /api/tasks/filter?done={done}&priority={priority}&dueDate={dueDate}
    @GetMapping("/filter")
    public ResponseEntity<?> filterTasks(@RequestParam(required = false) Boolean done,
                                  @RequestParam(required = false) Long priorityId,
                                  @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dueDate, @RequestParam String userId) {

        try {
            log.info("Trying to filter tasks with done: {}, priority: {}, dueDate: {} ", done, priorityId, dueDate);
            List<TaskItem> tasks = taskService.filterTasks(done, priorityId, dueDate, userId);
            if (tasks.isEmpty()) {
                log.error("404 failed to filter tasks. No tasks found with done: {}, priority: {}, dueDate: {} ", done, priorityId, dueDate);
                return new ResponseEntity<>(AuditConstants.NO_TASKS_FOUND, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(tasks, HttpStatus.OK);
        }
        catch (Exception e) {
            log.error("500 Failed to filter tasks with done: {}, priority: {}, dueDate: {} ", done, priorityId, dueDate);
            return new ResponseEntity<>(AuditConstants.FILTER_TASKS_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    GET /api/tasks/sort?sortBy={field}&direction={asc|desc}
    @GetMapping("/sort")
    public ResponseEntity<?> sortTasks(@RequestParam String sortBy,
                                @RequestParam(defaultValue = "asc") String direction, @RequestParam String userId) {

        try {
            log.info("Trying to sort tasks by: {}, direction: {} ", sortBy, direction);
            List<TaskItem> tasks = taskService.sortTasks(sortBy, direction, userId);
            if (tasks.isEmpty()) {
                log.error("404 failed to sort tasks. No tasks found with sortBy: {}, direction: {} ", sortBy, direction);
                return new ResponseEntity<>(AuditConstants.NO_TASKS_FOUND, HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>(tasks, HttpStatus.OK);
        }
        catch (Exception e) {
            log.error("500 Failed to sort tasks by: {}, direction: {} ", sortBy, direction);
            return new ResponseEntity<>(AuditConstants.SORT_TASKS_FAILED, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/{taskId}/attachments")
    public ResponseEntity<?> uploadMultipleFiles(@RequestParam("files") List<MultipartFile> files, @PathVariable Long taskId) {
        List<String> uploadedFileNames;
        try {
            log.info("Uploading files to task: {} ", taskId);
            uploadedFileNames = fileAttachmentService.uploadAttachmentsToTask(files, taskId);
        } catch (RuntimeException e) {
            log.error("400 Failed to upload files to task: {} ", taskId);
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            log.error("500 Failed to upload files to task: {} ", taskId);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not upload the files: " + e.getMessage());
        }
        return ResponseEntity.ok("Files uploaded successfully: " + uploadedFileNames);
    }


    
    @GetMapping("/{taskId}/attachments")
    public ResponseEntity<?> getAttachmentsByTaskId(@PathVariable Long taskId) {
        try {
            log.info("Retrieving attachments for task: {} ", taskId);
            List<FileAttachment> attachments = fileAttachmentService.getAttachmentsByTaskId(taskId);
            if (attachments.isEmpty()) {
                log.warn("No attachments found for task: {} ", taskId);
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(attachments);
        } catch (EntityNotFoundException e) {
            log.error("404 Failed to retrieve attachments for task: {} ", taskId);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            log.error("500 Failed to retrieve attachments for task: {} ", taskId);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred while retrieving attachments: " + e.getMessage());
        }
    }

    @DeleteMapping("/{taskId}/attachments/{attachmentId}")
    public ResponseEntity<?> deleteAttachment(@PathVariable Long taskId, @PathVariable Long attachmentId) {
        try {
            log.info("Deleting attachment: {} for task: {} ", attachmentId, taskId);
            String message = fileAttachmentService.deleteAttachment(taskId, attachmentId);
            return ResponseEntity.ok().body(message);
        } catch (EntityNotFoundException e) {
            log.error("404 Failed to delete attachment: {} for task: {} ", attachmentId, taskId);
            return ResponseEntity.notFound().build();
        } catch (IllegalArgumentException e) {
            log.error("400 Failed to delete attachment: {} for task: {} ", attachmentId, taskId);
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            log.error("500 Failed to delete attachment: {} for task: {} ", attachmentId, taskId);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error deleting attachment: " + e.getMessage());
        }
    }
}
