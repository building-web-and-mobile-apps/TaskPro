package de.hsfulda.bwmatodoapp.Task;

import de.hsfulda.bwmatodoapp.Category.Category;
import de.hsfulda.bwmatodoapp.Category.CategoryRepository;
import de.hsfulda.bwmatodoapp.File.FileAttachment;
import de.hsfulda.bwmatodoapp.Priority.Priority;
import de.hsfulda.bwmatodoapp.Priority.PriorityRepository;
import de.hsfulda.bwmatodoapp.User.UserModel;
import de.hsfulda.bwmatodoapp.User.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class TaskDataSeeder implements CommandLineRunner {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final PriorityRepository priorityRepository;

    public TaskDataSeeder(TaskRepository taskRepository, UserRepository userRepository, CategoryRepository categoryRepository, PriorityRepository priorityRepository) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.priorityRepository = priorityRepository;
    }

    @Override
    public void run(String... args) {
        // Only seed if there are no tasks
        if (taskRepository.count() == 0) {
            seedTasks();
        }
    }

    private void seedTasks() {
        UserModel user = userRepository.findById(1L)
                .orElseThrow(() -> new RuntimeException("User not found with id: 1"));

        Priority priorityLow = priorityRepository.findById(1L)
                .orElseThrow(() -> new RuntimeException("Priority not found with id: 1"));
        Priority priorityMedium = priorityRepository.findById(2L)
                .orElseThrow(() -> new RuntimeException("Priority not found with id: 2"));
        Priority priorityHigh = priorityRepository.findById(3L)
                .orElseThrow(() -> new RuntimeException("Priority not found with id: 3"));

        Category categoryPersonal = categoryRepository.findById(1L).orElseThrow(() -> new RuntimeException("Category not found with id: 1"));
        Category categoryWork = categoryRepository.findById(2L).orElseThrow(() -> new RuntimeException("Category not found with id: 2"));
        Category categoryOther = categoryRepository.findById(3L).orElseThrow(() -> new RuntimeException("Category not found with id: 3"));

        // Seeder 1: Grocery Shopping Task
        taskRepository.save(new TaskItem(
                null,
                "Grocery Shopping",
                "Buy groceries for the week including fruits, vegetables, and dairy.",
                false,
                LocalDateTime.now().plusDays(1),
                null,
                false,
                user,
                categoryPersonal,
                priorityHigh,
                new ArrayList<>()));

        // Seeder 2: Book Reading Task
        taskRepository.save(new TaskItem(
                null,
                "Read 'Atomic Habits'",
                "Finish reading 'Atomic Habits' by James Clear.",
                false,
                LocalDateTime.now().plusDays(7),
                null,
                false,
                user,
                categoryWork,
                priorityMedium,
                new ArrayList<>()));

        // Seeder 3: Exercise Routine
        taskRepository.save(new TaskItem(
                null,
                "Morning Exercise",
                "Complete a 30-minute workout session.",
                false,
                LocalDateTime.now().plusDays(2),
                null,
                false,
                user,
                categoryOther,
                priorityLow,
                new ArrayList<>()));

        // Seeder 4: Project Report Task
        taskRepository.save(new TaskItem(
                null,
                "Project Report",
                "Prepare the monthly project progress report.",
                false,
                LocalDateTime.now().plusDays(3),
                null,
                false,
                user,
                categoryWork,
                priorityHigh,
                new ArrayList<>()));

        // Seeder 5: Call Friend
        taskRepository.save(new TaskItem(
                null,
                "Call John",
                "Catch up call with John.",
                false,
                LocalDateTime.now().plusDays(4),
                null,
                false,
                user,
                categoryPersonal,
                priorityLow,
                new ArrayList<>()));
    }

}
