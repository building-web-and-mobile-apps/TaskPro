package de.hsfulda.bwmatodoapp.Audit;

import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
public class AuditService {

    private final AuditRepository auditRepository;

    public AuditService(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    public void log(String username, String activity) {
        log.trace("User '{}' performed activity: {}", username, activity);
    }

    public void audit(AuditItem auditItem) {
        auditRepository.save(auditItem);
    }

    public void auditRequest(HttpServletRequest request) {
        String url = request.getRequestURL().toString();
        String uri = request.getRequestURI();
        String method = request.getMethod();
        Map<String,String> parameters = getParameters(request);
        String userId = request.getUserPrincipal() != null ? request.getUserPrincipal().getName() : null;

        log(userId, "Request: " + method + " " + uri);

        AuditItem auditItem = new AuditItem();
        auditItem.setUrl(url);
        auditItem.setUri(uri);
        auditItem.setMethod(method);
        auditItem.setParameters(parameters.toString());
        auditItem.setStart_time(LocalDateTime.now());
        auditItem.setUser(userId);
        
        audit(auditItem);
    }

    private Map<String,String> getParameters(HttpServletRequest request) {
        Map<String,String> parameters = new HashMap<>();
        Enumeration<String> params = request.getParameterNames();
        while(params.hasMoreElements()) {
            String paramName = params.nextElement();
            String paramValue = request.getParameter(paramName);
            parameters.put(paramName,paramValue);
        }
        return parameters;
    }

}
