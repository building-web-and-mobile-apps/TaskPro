package de.hsfulda.bwmatodoapp.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserModel addUser(UserModel user) {
        return userRepository.save(user);
    }

    public UserModel updateUser(Long userId, UserModel userDetails) {
        UserModel user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("User not found with id: " + userId));
        user.setName(userDetails.getName());
        user.setEmail(userDetails.getEmail());
        user.setUpdatedAt(LocalDateTime.now());
        if (userDetails.getRole() != null) {
            user.setRole(userDetails.getRole());
        }
        return userRepository.save(user);
    }

    public UserModel findByUserId(String userId) {
        return userRepository.findByUserId(userId)
                .orElseThrow(() -> new RuntimeException("User not found with user id: " + userId));
    }

    public void deleteUser(String uid) {
        UserModel user = userRepository.findByUserId(uid)
                .orElseThrow(() -> new RuntimeException("User not found with user id: " + uid));
        userRepository.delete(user);
    }
}