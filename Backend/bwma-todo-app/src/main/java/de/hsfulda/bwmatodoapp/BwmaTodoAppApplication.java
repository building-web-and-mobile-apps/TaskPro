package de.hsfulda.bwmatodoapp;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;

import java.io.IOException;

@SpringBootApplication
@EnableAspectJAutoProxy
@EnableMethodSecurity(prePostEnabled = true)
public class BwmaTodoAppApplication {
	private static final Logger logger = LogManager.getLogger(BwmaTodoAppApplication.class);

	public static void main(String[] args) {
		logger.info("Starting Todo App");
		SpringApplication.run(BwmaTodoAppApplication.class, args);
	}
}
