package de.hsfulda.bwmatodoapp.File;

import de.hsfulda.bwmatodoapp.Task.TaskItem;
import de.hsfulda.bwmatodoapp.Task.TaskRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
public class FileAttachmentService {

    private final TaskRepository taskRepository;
    private final FileAttachmentRepository fileAttachmentRepository;
    private final FirebaseStorageService firebaseStorageService;

    @Autowired
    public FileAttachmentService(TaskRepository taskRepository,
                                 FileAttachmentRepository fileAttachmentRepository,
                                 FirebaseStorageService firebaseStorageService) {
        this.taskRepository = taskRepository;
        this.fileAttachmentRepository = fileAttachmentRepository;
        this.firebaseStorageService = firebaseStorageService;
    }

    public List<String> uploadAttachmentsToTask(List<MultipartFile> files, Long taskId) throws Exception {
        if (files.isEmpty()) {
            throw new IllegalArgumentException("No files provided.");
        }
        List<String> uploadedFileNames = new ArrayList<>();
        TaskItem task = taskRepository.findById(taskId).orElseThrow(() -> new RuntimeException("Task not found"));

        for (MultipartFile file : files) {
            String filePath = firebaseStorageService.uploadFile(file);
            FileAttachment attachment = new FileAttachment();
            attachment.setFileName(file.getOriginalFilename());
            attachment.setFileType(file.getContentType());
            attachment.setSize(file.getSize());
            attachment.setFilePath(filePath);
            attachment.setTask(task);
            fileAttachmentRepository.save(attachment);
            uploadedFileNames.add(file.getOriginalFilename());
        }
        return uploadedFileNames;
    }


    @Transactional
    public List<FileAttachment> getAttachmentsByTaskId(Long taskId) {
        TaskItem task = taskRepository.findById(taskId).orElseThrow(() -> new EntityNotFoundException("Task not found with ID: " + taskId));
        return new ArrayList<>(task.getAttachments());
    }


    @Transactional
    public String deleteAttachment(Long taskId, Long attachmentId) {
        TaskItem task = taskRepository.findById(taskId)
                .orElseThrow(() -> new EntityNotFoundException("Task not found with id: " + taskId));


        // Retrieve the attachment, ensuring it's associated with the given task
        FileAttachment attachment = fileAttachmentRepository.findById(attachmentId)
                .orElseThrow(() -> new EntityNotFoundException("Attachment not found with id: " + attachmentId));

        if (!attachment.getTask().getId().equals(taskId)) {
            throw new IllegalArgumentException("Attachment " + attachmentId + " does not belong to task " + taskId);
        }

        // Remove the attachment from the task's collection of attachments
        task.getAttachments().remove(attachment);

        // Delete the attachment
        fileAttachmentRepository.delete(attachment);

        return "Attachment file has successfully been deleted";
    }

}
