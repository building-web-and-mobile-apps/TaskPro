package de.hsfulda.bwmatodoapp.User;


import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "users")
public class UserModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;
    private String name;
    @Column(name = "userid")
    private String userId;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;
    @Column(name = "role", nullable = false, columnDefinition = "varchar(255) default 'user'")
    private String role = "user";
}
