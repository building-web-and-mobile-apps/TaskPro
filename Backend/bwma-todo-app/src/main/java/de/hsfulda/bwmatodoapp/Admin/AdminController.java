package de.hsfulda.bwmatodoapp.Admin;

import com.google.cloud.firestore.Firestore;
import com.google.firebase.auth.FirebaseAuth;
import de.hsfulda.bwmatodoapp.User.UserModel;
import de.hsfulda.bwmatodoapp.User.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/admin")
public class AdminController {
    private final UserService userService;

    public AdminController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("@authService.isAdmin()")
    @PostMapping("/add-admin")
    public ResponseEntity<?> addAdmin( @RequestBody Map<String, String> body) {
        String uid = body.get("uid");
        try {
            UserModel user = userService.findByUserId(uid);
            user.setRole("admin");
            userService.updateUser(user.getId(), user);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            String errorMessage = "Error setting custom claims for UID: " + uid + ". " + e.getMessage();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
        }
    }

    @PreAuthorize("@authService.isAdmin()")
    @PostMapping("/remove-admin")
    public ResponseEntity<?> removeAdmin( @RequestBody Map<String, String> body) {
        String uid = body.get("uid");
        try {
            UserModel user = userService.findByUserId(uid);
            user.setRole("user");
            userService.updateUser(user.getId(), user);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            String errorMessage = "Error setting custom claims for UID: " + uid + ". " + e.getMessage();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
        }
    }

    @PreAuthorize("@authService.isAdmin()")
    @PostMapping("/remove-account")
    public ResponseEntity<?> removeAccount(@RequestBody Map<String, String> body) {
        String uid = body.get("uid");
        try {
            userService.deleteUser(uid);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            String errorMessage = "Error deleting user with UID: " + uid + ". " + e.getMessage();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
        }
    }
}
