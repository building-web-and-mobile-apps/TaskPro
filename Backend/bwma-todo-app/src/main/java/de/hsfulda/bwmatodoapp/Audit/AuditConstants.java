package de.hsfulda.bwmatodoapp.Audit;

public class AuditConstants {
    public static final String NO_TASK_FOUND = "No task found";
    public static final String NO_TASKS_FOUND = "No tasks found";
    public static final String GET_TASKS = "Get tasks";
    public static final String GET_TASKS_FAILED = "Get tasks failed: ";
    public static final String ADD_TASK = "Add task ";
    public static final String ADD_TASK_FAILED = "Add task failed: ";
    public static final String ADD_TASKS = "Add tasks ";
    public static final String ADD_TASKS_FAILED = "Add tasks failed: ";
    public static final String UPDATE_TASK = "Update task ";
    public static final String UPDATE_TASK_FAILED = "Update task failed: ";
    public static final String DELETE_TASK = "Delete task ";
    public static final String DELETE_TASK_FAILED = "Delete task failed: ";
    public static final String GET_TASK = "Get task ";
    public static final String GET_TASK_FAILED = "Get task failed: ";
    public static final String SEARCH_TASKS = "Search tasks ";
    public static final String SEARCH_TASKS_FAILED = "Search tasks failed: ";
    public static final String FILTER_TASKS = "Filter tasks ";
    public static final String FILTER_TASKS_FAILED = "Filter tasks failed: ";
    public static final String SORT_TASKS = "Sort tasks ";
    public static final String SORT_TASKS_FAILED = "Sort tasks failed: ";
}