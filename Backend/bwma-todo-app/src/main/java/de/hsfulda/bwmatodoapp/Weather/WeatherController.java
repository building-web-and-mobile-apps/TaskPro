package de.hsfulda.bwmatodoapp.Weather;

import de.hsfulda.bwmatodoapp.config.WeatherConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/weather")
@Slf4j
public class WeatherController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private WeatherConfig weatherConfig;

    @GetMapping("/{city}")
    public ResponseEntity<String> getWeather(@PathVariable String city) {
        try {
            String apiKey = weatherConfig.getApiKey();
            String apiUrl = weatherConfig.getApiUrl() + "?q=" + city + "&appid=" + apiKey;
            String weatherData = restTemplate.getForObject(apiUrl, String.class);

            log.info("Get weather data for city: " + city);

            return ResponseEntity.ok(weatherData);

        } catch (HttpClientErrorException.NotFound e) {
            log.error("404 City not found: " + city);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("City not found");

        } catch (Exception e) {
            log.error("500 An error occurred while getting weather data for city: " + city);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occurred");
        }
    }

}