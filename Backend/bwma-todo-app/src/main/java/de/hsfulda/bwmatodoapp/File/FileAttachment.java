package de.hsfulda.bwmatodoapp.File;

import com.fasterxml.jackson.annotation.JsonBackReference;
import de.hsfulda.bwmatodoapp.Task.TaskItem;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "files")
public class FileAttachment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String fileName;
    private String fileType;
    private long size;

    // Optional: Link this to your Task entity
    @ManyToOne
    @JoinColumn(name = "task_id")
    @JsonBackReference
    private TaskItem task;

    // For simplicity, storing file path; consider URL for cloud storage
    private String filePath;

}
