package de.hsfulda.bwmatodoapp.Task;

import de.hsfulda.bwmatodoapp.Priority.Priority;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class TaskSpecification {

    public static Specification<TaskItem> isDone(boolean done) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("done"), done);
    }

    public static Specification<TaskItem> hasPriority(Priority priority) {
        return (root, query, criteriaBuilder) -> {
            if (priority == null) {
                return criteriaBuilder.isTrue(criteriaBuilder.literal(true)); // always true
            }
            return criteriaBuilder.equal(root.get("priority"), priority);
        };
    }

    public static Specification<TaskItem> isDueBy(LocalDate dueDate) {
        return (root, query, criteriaBuilder) -> {
            if (dueDate == null) {
                return criteriaBuilder.isTrue(criteriaBuilder.literal(true)); // always true
            }
            return criteriaBuilder.lessThanOrEqualTo(root.get("dueDate"), dueDate);
        };
    }
}
