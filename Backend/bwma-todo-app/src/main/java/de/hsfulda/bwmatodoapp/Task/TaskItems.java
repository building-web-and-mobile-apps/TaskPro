package de.hsfulda.bwmatodoapp.Task;

import java.util.List;

public class TaskItems {
    private List<TaskItem> taskItems;

    public void setTaskItems(List<TaskItem> taskItems) {
        this.taskItems = taskItems;
    }

    public List<TaskItem> getTaskItems() {
        return taskItems;
    }
}
