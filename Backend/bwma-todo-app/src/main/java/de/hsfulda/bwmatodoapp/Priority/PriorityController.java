package de.hsfulda.bwmatodoapp.Priority;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/priorities")
@Slf4j
public class PriorityController {
    private final PriorityRepository priorityRepository;

    public PriorityController(PriorityRepository priorityRepository) {
        this.priorityRepository = priorityRepository;
    }

    @GetMapping
    public List<Priority> getAllPriorities() {
        log.info("Trying to get all priorities");
        return priorityRepository.findAll();
    }
}
