package de.hsfulda.bwmatodoapp.User;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService){
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<?> addUser(@RequestBody UserModel user) {
        try {
            log.info("Trying to add user: " + user);
            UserModel newUser = userService.addUser(user);
            return new ResponseEntity<>(newUser, HttpStatus.CREATED);
        } catch (Exception e) {
            log.error("500 Add user failed with user: " + user);
            return new ResponseEntity<>("Add user failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{userId}")
    public ResponseEntity<?> updateUser(@PathVariable Long userId, @RequestBody UserModel userDetails) {
        try {
            log.info("Trying to update user with id: " + userId + " with details: " + userDetails);
            UserModel updatedUser = userService.updateUser(userId, userDetails);
            return new ResponseEntity<>(updatedUser, HttpStatus.OK);
        } catch (Exception e) {
            log.error("500 Update user failed with id: " + userId + " with details: " + userDetails);
            return new ResponseEntity<>("Update user failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    public ResponseEntity<?> getUserByUserId(@RequestParam String userId) {
        try {
            log.info("Trying to get user with id: " + userId);
            UserModel user = userService.findByUserId(userId);
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            log.error("500 Get user failed with id: " + userId);
            return new ResponseEntity<>("Get user failed", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
