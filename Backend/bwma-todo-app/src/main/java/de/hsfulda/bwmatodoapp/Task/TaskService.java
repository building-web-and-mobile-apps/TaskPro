package de.hsfulda.bwmatodoapp.Task;

import de.hsfulda.bwmatodoapp.Category.Category;
import de.hsfulda.bwmatodoapp.Category.CategoryRepository;
import de.hsfulda.bwmatodoapp.Priority.Priority;
import de.hsfulda.bwmatodoapp.Priority.PriorityRepository;
import de.hsfulda.bwmatodoapp.User.UserModel;
import de.hsfulda.bwmatodoapp.User.UserRepository;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class TaskService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final PriorityRepository priorityRepository;
    private final CategoryRepository categoryRepository;

    public TaskService(TaskRepository taskRepository,
                       UserRepository userRepository,
                       PriorityRepository priorityRepository,
                       CategoryRepository categoryRepository) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
        this.priorityRepository = priorityRepository;
        this.categoryRepository = categoryRepository;
    }

    public List<TaskItem> getTasks() {
        return taskRepository.findAll();
    }

    public TaskItem getTask(Long id) {
        Optional<TaskItem> task = taskRepository.findById(id);
        if (task.isPresent()) {
            return task.get();
        }
        return null;
    }

    public TaskItem addTask(TaskItem taskItem) {
        String userId = taskItem.getUser().getUserId();
        Long categoryId = taskItem.getCategory().getId();
        Long priorityId = taskItem.getPriority().getId();

        Optional<UserModel> userOptional = userRepository.findByUserId(userId);
        Optional<Category> categoryOptional = categoryRepository.findById(categoryId);
        Optional<Priority> priorityOptional = priorityRepository.findById(priorityId);

        if (userOptional.isEmpty() || categoryOptional.isEmpty() || priorityOptional.isEmpty()){
            return null;
        }

        UserModel user = userOptional.get();
        taskItem.setUser(user);

        Category category = categoryOptional.get();
        taskItem.setCategory(category);

        Priority priority = priorityOptional.get();
        taskItem.setPriority(priority);

        TaskItem task = taskRepository.save(taskItem);
        return task;
    }

    public List<TaskItem> addTasks(List<TaskItem> taskItems) {
        List<TaskItem> tasksAdded = new ArrayList<>();
        for (TaskItem taskItem : taskItems) {
            TaskItem task = addTask(taskItem);
            if (task != null) {
                tasksAdded.add(task);
            }
        }
        return tasksAdded;
    }

    public TaskItem updateTask(Long id) {
        Optional<TaskItem> task = taskRepository.findById(id);
        if (task.isPresent()) {
            TaskItem taskToUpdate = task.get();
            boolean done = taskToUpdate.isDone();
            taskToUpdate.setDone(!done);
            taskRepository.save(taskToUpdate);
            return taskToUpdate;
        }
        return null;
    }

    public TaskItem updateTask(Long id, TaskItem taskItem) {
        Optional<TaskItem> task = taskRepository.findById(id);
        if (task.isPresent()) {
            TaskItem taskToUpdate = task.get();

            taskToUpdate.setTitle(taskItem.getTitle());
            taskToUpdate.setDescription(taskItem.getDescription());
            taskToUpdate.setDone(taskItem.isDone());
            taskToUpdate.setDueDate(taskItem.getDueDate());
            taskToUpdate.setCompleteDate(taskItem.getCompleteDate());
            taskToUpdate.setArchived(taskItem.isArchived());

            String userId = taskItem.getUser().getUserId();
            Optional<UserModel> userOptional = userRepository.findByUserId(userId);
            if (userOptional.isEmpty()) {
                return null;
            }
            UserModel user = userOptional.get();
            taskToUpdate.setUser(user);

            Long categoryId = taskItem.getCategory().getId();
            Optional<Category> categoryOptional = categoryRepository.findById(categoryId);
            if (categoryOptional.isEmpty()) {
                return null;
            }
            Category category = categoryOptional.get();
            taskToUpdate.setCategory(category);

            Long priorityId = taskItem.getPriority().getId();
            Optional<Priority> priorityOptional = priorityRepository.findById(priorityId);
            if (priorityOptional.isEmpty()) {
                return null;
            }
            Priority priority = priorityOptional.get();
            taskToUpdate.setPriority(priority);

            return taskRepository.save(taskToUpdate);
        }
        return null;
    }

    public void deleteTask(Long id) {
        taskRepository.deleteById(id);
    }

    public List<TaskItem> searchTasks(String keyword, String userId) {
        List<TaskItem> tasks = taskRepository.findByKeyword(keyword);
        tasks.removeIf(task -> task.getUser() == null);
        tasks.removeIf(task -> !task.getUser().getUserId().equals(userId));
        return tasks;
    }

    public List<TaskItem> filterTasks(Boolean done, Long priorityId, LocalDate dueDate, String userId) {
        Specification<TaskItem> spec = Specification.where(
                done == null ? null : TaskSpecification.isDone(done)
        );

        if (priorityId != null) {
            Priority priority = priorityRepository.findById(priorityId).orElseThrow(
                    () -> new RuntimeException("Priority not found with id: " + priorityId)
            );
            spec = spec.and(TaskSpecification.hasPriority(priority));
        }

        if (dueDate != null) {
            spec = spec.and(TaskSpecification.isDueBy(dueDate));
        }


        List<TaskItem> tasks = taskRepository.findAll(spec);

        //Filter null user
        tasks.removeIf(task -> task.getUser() == null);
        // Filter by user
        tasks.removeIf(task -> !task.getUser().getUserId().equals(userId));

        return tasks;
    }

    public List<TaskItem> sortTasks(String sortBy, String direction, String userId) {
        Sort sort = Sort.by(Sort.Direction.fromString(direction), sortBy);
        List<TaskItem> tasks = taskRepository.findAll(sort);

        //Filter null user
        tasks.removeIf(task -> task.getUser() == null);
        // Filter by user
        tasks.removeIf(task -> !task.getUser().getUserId().equals(userId));

        return tasks;
    }

}
