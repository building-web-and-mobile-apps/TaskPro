package de.hsfulda.bwmatodoapp.Audit;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity @Data @NoArgsConstructor @AllArgsConstructor
@Table(name = "audit")
public class AuditItem {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;
        private String url;
        private String uri;
        private String method;
        private String parameters;
        private LocalDateTime start_time;
        private String user;
}
