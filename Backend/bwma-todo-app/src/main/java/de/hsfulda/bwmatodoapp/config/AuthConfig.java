package de.hsfulda.bwmatodoapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import de.hsfulda.bwmatodoapp.auth.AuthTokenFilter;

@Configuration
@EnableWebSecurity
public class AuthConfig {

    @Bean
    public AuthTokenFilter firebaseTokenFilter() {
        return new AuthTokenFilter();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf(csrf -> csrf.disable()
                .addFilterBefore(firebaseTokenFilter(), UsernamePasswordAuthenticationFilter.class));

        return http.build();
    }
}
