package de_hsfulda_bwmatodoapp.User;

import de.hsfulda.bwmatodoapp.User.UserModel;
import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import static org.assertj.core.api.Assertions.assertThat;

class UserModelTest {

    @Test
    void testIdField() {
        UserModel user = new UserModel();
        user.setId(1L);
        assertThat(user.getId()).isEqualTo(1L);
    }

    @Test
    void testEmailField() {
        UserModel user = new UserModel();
        user.setEmail("test@example.com");
        assertThat(user.getEmail()).isEqualTo("test@example.com");
    }

    @Test
    void testNameField() {
        UserModel user = new UserModel();
        user.setName("Test User");
        assertThat(user.getName()).isEqualTo("Test User");
    }

    @Test
    void testUserIdField() {
        UserModel user = new UserModel();
        user.setUserId("test123");
        assertThat(user.getUserId()).isEqualTo("test123");
    }

    @Test
    void testCreatedAtField() {
        UserModel user = new UserModel();
        LocalDateTime createdAt = LocalDateTime.of(2022, 1, 1, 10, 30);
        user.setCreatedAt(createdAt);
        assertThat(user.getCreatedAt()).isEqualTo(createdAt);
    }

    @Test
    void testUpdatedAtField() {
        UserModel user = new UserModel();
        LocalDateTime updatedAt = LocalDateTime.of(2022, 1, 2, 11, 45);
        user.setUpdatedAt(updatedAt);
        assertThat(user.getUpdatedAt()).isEqualTo(updatedAt);
    }

    @Test
    void testToString() {
        UserModel user = new UserModel();
        user.setId(1L);
        user.setEmail("test@example.com");
        user.setName("Test User");
        user.setUserId("test123");
        LocalDateTime createdAt = LocalDateTime.of(2022, 1, 1, 10, 30);
        user.setCreatedAt(createdAt);
        LocalDateTime updatedAt = LocalDateTime.of(2022, 1, 2, 11, 45);
        user.setUpdatedAt(updatedAt);

        assertThat(user.toString()).isEqualTo("UserModel(id=1, email=test@example.com, name=Test User, userId=test123, createdAt=2022-01-01T10:30, updatedAt=2022-01-02T11:45, role=user)");
    }
}
