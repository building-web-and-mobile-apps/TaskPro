package de_hsfulda_bwmatodoapp.User;

import de.hsfulda.bwmatodoapp.User.UserModel;
import de.hsfulda.bwmatodoapp.User.UserRepository;
import de.hsfulda.bwmatodoapp.User.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDateTime;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void testAddUser() {
        UserModel user = new UserModel();
        user.setEmail("test@example.com");
        user.setName("Test User");
        user.setUserId("test123");
        LocalDateTime now = LocalDateTime.now();
        user.setCreatedAt(now);
        user.setUpdatedAt(now);

        when(userRepository.save(any(UserModel.class))).thenReturn(user);

        UserModel savedUser = userService.addUser(user);

        assertEquals(user, savedUser);
        verify(userRepository, times(1)).save(any(UserModel.class));
    }

    @Test
    void testUpdateUser() {
        Long userId = 1L;
        UserModel userDetails = new UserModel();
        userDetails.setName("Updated Name");
        userDetails.setEmail("updated@example.com");

        UserModel existingUser = new UserModel();
        existingUser.setId(userId);
        existingUser.setName("Old Name");
        existingUser.setEmail("old@example.com");
        existingUser.setCreatedAt(LocalDateTime.now());
        existingUser.setUpdatedAt(LocalDateTime.now());

        UserModel updatedUser = new UserModel();
        updatedUser.setId(userId);
        updatedUser.setName("Updated Name");
        updatedUser.setEmail("updated@example.com");
        updatedUser.setUpdatedAt(LocalDateTime.now());

        when(userRepository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userRepository.save(existingUser)).thenReturn(updatedUser);

        UserModel updatedUserFinal = userService.updateUser(userId, userDetails);

        assertEquals("Updated Name", updatedUserFinal.getName());
        assertEquals("updated@example.com", updatedUserFinal.getEmail());
        verify(userRepository, times(1)).findById(userId);
        verify(userRepository, times(1)).save(existingUser);
    }

    @Test
    void testUpdateUser_UserNotFound() {
        // Prepare test data
        Long userId = 1L;
        UserModel userDetails = new UserModel();

        // Mock userRepository.findById() method to return empty optional
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // Call the updateUser method and verify if it throws RuntimeException
        assertThrows(RuntimeException.class, () -> userService.updateUser(userId, userDetails));
    }

    @Test
    void testFindByUserId() {
        // Prepare test data
        String userId = "test123";
        UserModel user = new UserModel();
        user.setId(1L);
        user.setEmail("test@example.com");
        user.setName("Test User");
        user.setUserId(userId);
        LocalDateTime createdAt = LocalDateTime.now();
        user.setCreatedAt(createdAt);
        LocalDateTime updatedAt = LocalDateTime.now();
        user.setUpdatedAt(updatedAt);

        // Mock userRepository.findByUserId() method
        when(userRepository.findByUserId(userId)).thenReturn(Optional.of(user));

        // Call the findByUserId method
        UserModel foundUser = userService.findByUserId(userId);

        // Verify the result
        assertEquals(user, foundUser);
        verify(userRepository, times(1)).findByUserId(userId);
    }

    @Test
    void testFindByUserId_UserNotFound() {
        // Prepare test data
        String userId = "test123";

        // Mock userRepository.findByUserId() method to return empty optional
        when(userRepository.findByUserId(userId)).thenReturn(Optional.empty());

        // Call the findByUserId method and verify if it throws RuntimeException
        assertThrows(RuntimeException.class, () -> userService.findByUserId(userId));
    }
}
