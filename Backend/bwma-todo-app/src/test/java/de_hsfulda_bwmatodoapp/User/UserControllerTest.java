package de_hsfulda_bwmatodoapp.User;

import de.hsfulda.bwmatodoapp.User.UserController;
import de.hsfulda.bwmatodoapp.User.UserModel;
import de.hsfulda.bwmatodoapp.User.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;

    @Test
    void testAddUser() {
        UserModel user = new UserModel();
        user.setEmail("test@example.com");
        user.setName("Test User");
        user.setUserId("test123");
        user.setCreatedAt(LocalDateTime.now());
        user.setUpdatedAt(LocalDateTime.now());

        when(userService.addUser(any(UserModel.class))).thenReturn(user);

        ResponseEntity<?> responseEntity = userController.addUser(user);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());
    }

    @Test
    void testUpdateUser() {
        UserModel user = new UserModel();
        user.setId(1L);
        user.setEmail("test@example.com");
        user.setName("Test User");
        user.setUserId("test123");
        user.setCreatedAt(LocalDateTime.now());
        user.setUpdatedAt(LocalDateTime.now());

        when(userService.updateUser(anyLong(), any(UserModel.class))).thenReturn(user);

        ResponseEntity<?> responseEntity = userController.updateUser(1L, user);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());
    }

    @Test
    void testGetUserByUserId() {
        UserModel user = new UserModel();
        user.setId(1L);
        user.setEmail("test@example.com");
        user.setName("Test User");
        user.setUserId("test123");
        user.setCreatedAt(LocalDateTime.now());
        user.setUpdatedAt(LocalDateTime.now());

        when(userService.findByUserId(anyString())).thenReturn(user);

        ResponseEntity<?> responseEntity = userController.getUserByUserId("test123");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());
    }

    @Test
    void testAddUser_Exception() {
        // Mocking the UserService to throw an exception when addUser is called
        when(userService.addUser(any(UserModel.class))).thenThrow(new RuntimeException("Failed to add user"));

        // Call the addUser method and verify the response
        ResponseEntity<?> responseEntity = userController.addUser(new UserModel());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertEquals("Add user failed", responseEntity.getBody());
    }

    @Test
    void testUpdateUser_Exception() {
        // Mocking the UserService to throw an exception when updateUser is called
        when(userService.updateUser(anyLong(), any(UserModel.class))).thenThrow(new RuntimeException("Failed to update user"));

        // Call the updateUser method and verify the response
        ResponseEntity<?> responseEntity = userController.updateUser(1L, new UserModel());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertEquals("Update user failed", responseEntity.getBody());
    }

    @Test
    void testGetUserByUserId_Exception() {
        // Mocking the UserService to throw an exception when findByUserId is called
        when(userService.findByUserId(anyString())).thenThrow(new RuntimeException("Failed to get user"));

        // Call the getUserByUserId method and verify the response
        ResponseEntity<?> responseEntity = userController.getUserByUserId("test123");
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertEquals("Get user failed", responseEntity.getBody());
    }
}
