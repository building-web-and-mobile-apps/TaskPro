package de_hsfulda_bwmatodoapp.Weather;

import de.hsfulda.bwmatodoapp.Weather.WeatherController;
import de.hsfulda.bwmatodoapp.config.WeatherConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class WeatherControllerTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private WeatherConfig weatherConfig;

    @InjectMocks
    private WeatherController weatherController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetWeather_Success() {
        String city = "Berlin";
        String apiKey = "your_api_key";
        String apiUrl = "http://api.openweathermap.org/data/2.5/weather";
        String weatherData = "Weather data for Berlin";

        when(weatherConfig.getApiKey()).thenReturn(apiKey);
        when(weatherConfig.getApiUrl()).thenReturn(apiUrl);
        when(restTemplate.getForObject(apiUrl + "?q=" + city + "&appid=" + apiKey, String.class)).thenReturn(weatherData);

        ResponseEntity<String> response = weatherController.getWeather(city);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(weatherData, response.getBody());
    }

    @Test
     void testGetWeather_CityNotFound() {
        String city = "NonExistentCity";
        String apiKey = "your_api_key";
        String apiUrl = "http://api.openweathermap.org/data/2.5/weather";

        when(weatherConfig.getApiKey()).thenReturn(apiKey);
        when(weatherConfig.getApiUrl()).thenReturn(apiUrl);
        when(restTemplate.getForObject(apiUrl + "?q=" + city + "&appid=" + apiKey, String.class))
                .thenThrow(HttpClientErrorException.NotFound.class);

        ResponseEntity<String> response = weatherController.getWeather(city);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("City not found", response.getBody());
    }

    @Test
    void testGetWeather_InternalServerError() {
        String city = "Berlin";
        String apiKey = "your_api_key";
        String apiUrl = "http://api.openweathermap.org/data/2.5/weather";

        when(weatherConfig.getApiKey()).thenReturn(apiKey);
        when(weatherConfig.getApiUrl()).thenReturn(apiUrl);
        when(restTemplate.getForObject(apiUrl + "?q=" + city + "&appid=" + apiKey, String.class))
                .thenThrow(RuntimeException.class);

        ResponseEntity<String> response = weatherController.getWeather(city);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals("An error occurred", response.getBody());
    }
}
