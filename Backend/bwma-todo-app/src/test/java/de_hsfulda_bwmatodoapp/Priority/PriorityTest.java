package de_hsfulda_bwmatodoapp.Priority;

import de.hsfulda.bwmatodoapp.Priority.Priority;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class PriorityTest {

    @Test
    void testNoArgsConstructor() {
        Priority priority = new Priority();
        assertNull(priority.getId());
        assertNull(priority.getName());
    }

    @Test
    void testAllArgsConstructor() {
        Long id = 1L;
        String name = "High";
        Priority priority = new Priority(id, name);
        assertEquals(id, priority.getId());
        assertEquals(name, priority.getName());
    }

    @Test
    void testGettersAndSetters() {
        Long id = 1L;
        String name = "Medium";
        Priority priority = new Priority();

        priority.setId(id);
        priority.setName(name);

        assertEquals(id, priority.getId());
        assertEquals(name, priority.getName());
    }
}
