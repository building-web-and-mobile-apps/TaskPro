package de_hsfulda_bwmatodoapp.Priority;

import de.hsfulda.bwmatodoapp.Priority.Priority;
import de.hsfulda.bwmatodoapp.Priority.PriorityController;
import de.hsfulda.bwmatodoapp.Priority.PriorityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

 class PriorityControllerTest {

    @Mock
    private PriorityRepository priorityRepository;

    @InjectMocks
    private PriorityController priorityController;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllPriorities() {
        // Given
        Priority priority1 = new Priority(1L, "High");
        Priority priority2 = new Priority(2L, "Medium");
        List<Priority> priorities = Arrays.asList(priority1, priority2);

        // Mock the repository method call
        when(priorityRepository.findAll()).thenReturn(priorities);

        // When
        List<Priority> result = priorityController.getAllPriorities();

        // Then
        assertEquals(priorities.size(), result.size());
        assertEquals(priorities.get(0), result.get(0));
        assertEquals(priorities.get(1), result.get(1));
    }
}
