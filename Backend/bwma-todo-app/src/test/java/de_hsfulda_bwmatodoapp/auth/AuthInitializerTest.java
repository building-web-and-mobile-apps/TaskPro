package de_hsfulda_bwmatodoapp.auth;

import com.google.firebase.FirebaseApp;
import de.hsfulda.bwmatodoapp.auth.AuthInitializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.*;

class AuthInitializerTest {

    @Mock
    private FirebaseApp firebaseApp;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testInitialize_WhenFirebaseAppNotInitialized_InitializesFirebaseApp() {
        AuthInitializer authInitializer = new AuthInitializer();
        authInitializer.initialize();
        verify(firebaseApp, times(1));
    }

    @Test
    void testInitialize_WhenFirebaseAppAlreadyInitialized_DoesNotInitializeFirebaseApp() {
        AuthInitializer authInitializer = new AuthInitializer();
        authInitializer.initialize();
        assertFalse(FirebaseApp.getApps().isEmpty());
    }
}
