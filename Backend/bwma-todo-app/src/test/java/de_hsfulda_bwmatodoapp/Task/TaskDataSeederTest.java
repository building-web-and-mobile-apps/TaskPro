package de_hsfulda_bwmatodoapp.Task;

import de.hsfulda.bwmatodoapp.Category.Category;
import de.hsfulda.bwmatodoapp.Category.CategoryRepository;
import de.hsfulda.bwmatodoapp.Priority.Priority;
import de.hsfulda.bwmatodoapp.Priority.PriorityRepository;
import de.hsfulda.bwmatodoapp.Task.TaskDataSeeder;
import de.hsfulda.bwmatodoapp.Task.TaskItem;
import de.hsfulda.bwmatodoapp.Task.TaskRepository;
import de.hsfulda.bwmatodoapp.User.UserModel;
import de.hsfulda.bwmatodoapp.User.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.Optional;

import static org.mockito.Mockito.*;

class TaskDataSeederTest {

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private PriorityRepository priorityRepository;

    private TaskDataSeeder taskDataSeeder;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        taskDataSeeder = new TaskDataSeeder(taskRepository, userRepository, categoryRepository, priorityRepository);
    }

    @Test
    void run_NoTasksToSeed() {
        when(taskRepository.count()).thenReturn(5L);

        taskDataSeeder.run();

        verify(taskRepository, never()).save(any());
    }

    @Test
    void run_SeedsTasks_IfNoTasksExist() {
        TaskDataSeeder taskDataSeeder = new TaskDataSeeder(taskRepository, userRepository, categoryRepository, priorityRepository);
        when(taskRepository.count()).thenReturn(0L);
        when(userRepository.findById(1L)).thenReturn(Optional.of(new UserModel()));
        when(priorityRepository.findById(anyLong())).thenReturn(Optional.of(new Priority()));
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(new Category()));

        taskDataSeeder.run();

        verify(taskRepository, times(5)).save(any(TaskItem.class));
    }

    @Test
    void run_DoesNotSeedTasks_IfTasksExist() {
        TaskDataSeeder taskDataSeeder = new TaskDataSeeder(taskRepository, userRepository, categoryRepository, priorityRepository);
        when(taskRepository.count()).thenReturn(5L);

        taskDataSeeder.run();

        verify(taskRepository, never()).save(any(TaskItem.class));
    }
}
