package de_hsfulda_bwmatodoapp.Task;

import de.hsfulda.bwmatodoapp.Priority.Priority;
import de.hsfulda.bwmatodoapp.Task.TaskItem;
import de.hsfulda.bwmatodoapp.Task.TaskSpecification;
import jakarta.persistence.criteria.*;
import org.junit.jupiter.api.Test;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class TaskSpecificationTest {

    @Test
    void isDone_True_ReturnsSpecification() {
        Specification<TaskItem> result = TaskSpecification.isDone(true);
        assertNotNull(result);
    }

    @Test
    void isDone_False_ReturnsSpecification() {
        Specification<TaskItem> result = TaskSpecification.isDone(false);
        assertNotNull(result);
    }

    @Test
    void hasPriority_WithNonNullPriority_ReturnsSpecification() {
        Priority priority = new Priority();
        Specification<TaskItem> result = TaskSpecification.hasPriority(priority);
        assertNotNull(result);
    }

    @Test
    void hasPriority_WithNullPriority_ReturnsSpecification() {
        Specification<TaskItem> result = TaskSpecification.hasPriority(null);
        assertNotNull(result);
    }

    @Test
    void isDueBy_WithNonNullDueDate_ReturnsSpecification() {
        LocalDate dueDate = LocalDate.now();
        Specification<TaskItem> result = TaskSpecification.isDueBy(dueDate);
        assertNotNull(result);
    }

    @Test
    void isDueBy_WithNullDueDate_ReturnsSpecification() {
        Specification<TaskItem> result = TaskSpecification.isDueBy(null);
        assertNotNull(result);
    }

    @Test
    void isDone_CriteriaBuilderUsed_CorrectPredicateGenerated() {
        Specification<TaskItem> specification = TaskSpecification.isDone(true);
        CriteriaBuilder criteriaBuilder = mock(CriteriaBuilder.class);
        Root<TaskItem> root = mock(Root.class);
        Predicate predicate = mock(Predicate.class);
        when(criteriaBuilder.equal(root.get("done"), true)).thenReturn(predicate);

        Predicate result = specification.toPredicate(root, null, criteriaBuilder);

        assertNotNull(result);
        verify(criteriaBuilder).equal(root.get("done"), true);
    }

}
