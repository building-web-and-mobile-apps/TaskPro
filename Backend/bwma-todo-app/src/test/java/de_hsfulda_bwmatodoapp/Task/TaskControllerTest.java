package de_hsfulda_bwmatodoapp.Task;

import de.hsfulda.bwmatodoapp.Audit.AuditConstants;
import de.hsfulda.bwmatodoapp.Task.TaskController;
import de.hsfulda.bwmatodoapp.Task.TaskItem;
import de.hsfulda.bwmatodoapp.Task.TaskItems;
import de.hsfulda.bwmatodoapp.Task.TaskService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class TaskControllerTest {

    @Mock
    private TaskService taskService;

    @InjectMocks
    private TaskController taskController;

    TaskControllerTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetTasks() {
        List<TaskItem> tasks = new ArrayList<>();
        when(taskService.getTasks()).thenReturn(tasks);

        ResponseEntity<?> response = taskController.getTasks();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(tasks, response.getBody());
    }

    @Test
    void testAddTask() {
        TaskItem taskItem = new TaskItem();
        when(taskService.addTask(any(TaskItem.class))).thenReturn(taskItem);

        ResponseEntity<?> response = taskController.addTask(taskItem);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(taskItem, response.getBody());
    }

    @Test
    void testAddTasks() {
        List<TaskItem> taskItems = new ArrayList<>();
        TaskItem taskItem = new TaskItem();
        taskItems.add(taskItem);
        TaskItems taskItemsWrapper = new TaskItems();
        taskItemsWrapper.setTaskItems(taskItems);
        when(taskService.addTasks(taskItems)).thenReturn(taskItems);

        ResponseEntity<?> response = taskController.addTasks(taskItemsWrapper);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(taskItems, response.getBody());
    }

    @Test
    void testUpdateTask_Success() {
        Long taskId = 1L;
        TaskItem updatedTask = new TaskItem();
        when(taskService.updateTask(anyLong())).thenReturn(updatedTask);

        ResponseEntity<?> response = taskController.updateTask(taskId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedTask, response.getBody());
    }

    @Test
    void testUpdateTask_Failure() {
        Long taskId = 1L;
        when(taskService.updateTask(anyLong())).thenReturn(null);

        ResponseEntity<?> response = taskController.updateTask(taskId);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.UPDATE_TASK_FAILED, response.getBody());
    }

    @Test
    void testUpdateTaskWithTaskItem_Success() {
        Long taskId = 1L;
        TaskItem updatedTask = new TaskItem();
        when(taskService.updateTask(eq(taskId), any(TaskItem.class))).thenReturn(updatedTask);

        ResponseEntity<?> response = taskController.updateTask(taskId, new TaskItem());

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(updatedTask, response.getBody());
    }

    @Test
    void testUpdateTaskWithTaskItem_Failure() {
        Long taskId = 1L;
        when(taskService.updateTask(eq(taskId), any(TaskItem.class))).thenReturn(null);

        ResponseEntity<?> response = taskController.updateTask(taskId, new TaskItem());

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.UPDATE_TASK_FAILED, response.getBody());
    }

    @Test
    void testDeleteTask_ExistingTask_Success() {
        Long taskId = 1L;
        TaskItem taskItem = new TaskItem();
        when(taskService.getTask(taskId)).thenReturn(taskItem);

        ResponseEntity<?> response = taskController.deleteTask(taskId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(AuditConstants.DELETE_TASK + taskId, response.getBody());
        verify(taskService, times(1)).deleteTask(taskId);
    }

    @Test
    void testDeleteTask_NonExistingTask_Failure() {
        Long taskId = 1L;
        when(taskService.getTask(taskId)).thenReturn(null);

        ResponseEntity<?> response = taskController.deleteTask(taskId);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.NO_TASK_FOUND, response.getBody());
        verify(taskService, never()).deleteTask(taskId);
    }

    @Test
    void testGetTask_ExistingTask_Success() {
        Long taskId = 1L;
        TaskItem taskItem = new TaskItem();
        when(taskService.getTask(taskId)).thenReturn(taskItem);

        ResponseEntity<?> response = taskController.getTask(taskId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(taskItem, response.getBody());
    }

    @Test
    void testGetTask_NonExistingTask_Failure() {
        Long taskId = 1L;
        when(taskService.getTask(taskId)).thenReturn(null);

        ResponseEntity<?> response = taskController.getTask(taskId);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.NO_TASK_FOUND, response.getBody());
    }

    @Test
    void testSearchTasks_TasksFound_Success() {
        String keyword = "test";
        String userId = "user123";
        List<TaskItem> foundTasks = new ArrayList<>();
        foundTasks.add(new TaskItem());
        when(taskService.searchTasks(keyword, userId)).thenReturn(foundTasks);

        ResponseEntity<?> response = taskController.searchTasks(keyword, userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(foundTasks, response.getBody());
    }

    @Test
    void testSearchTasks_NoTasksFound_Failure() {
        String keyword = "test";
        String userId = "user123";
        when(taskService.searchTasks(keyword, userId)).thenReturn(new ArrayList<>());

        ResponseEntity<?> response = taskController.searchTasks(keyword, userId);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.NO_TASKS_FOUND, response.getBody());
    }

    @Test
    void testSearchTasks_Exception_InternalServerError() {
        String keyword = "test";
        String userId = "user123";
        when(taskService.searchTasks(keyword, userId)).thenThrow(RuntimeException.class);

        ResponseEntity<?> response = taskController.searchTasks(keyword, userId);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.SEARCH_TASKS_FAILED, response.getBody());
    }

    @Test
    void testFilterTasks_TasksFound_Success() {
        Boolean done = true;
        Long priorityId = 1L;
        LocalDate dueDate = LocalDate.now();
        String userId = "user123";
        List<TaskItem> foundTasks = new ArrayList<>();
        foundTasks.add(new TaskItem());
        when(taskService.filterTasks(done, priorityId, dueDate, userId)).thenReturn(foundTasks);

        ResponseEntity<?> response = taskController.filterTasks(done, priorityId, dueDate, userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(foundTasks, response.getBody());
    }

    @Test
    void testFilterTasks_NoTasksFound_Failure() {
        Boolean done = true;
        Long priorityId = 1L;
        LocalDate dueDate = LocalDate.now();
        String userId = "user123";
        when(taskService.filterTasks(done, priorityId, dueDate, userId)).thenReturn(new ArrayList<>());

        ResponseEntity<?> response = taskController.filterTasks(done, priorityId, dueDate, userId);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.NO_TASKS_FOUND, response.getBody());
    }

    @Test
    void testFilterTasks_Exception_InternalServerError() {
        Boolean done = true;
        Long priorityId = 1L;
        LocalDate dueDate = LocalDate.now();
        String userId = "user123";
        when(taskService.filterTasks(done, priorityId, dueDate, userId)).thenThrow(RuntimeException.class);

        ResponseEntity<?> response = taskController.filterTasks(done, priorityId, dueDate, userId);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.FILTER_TASKS_FAILED, response.getBody());
    }

    @Test
    void testSortTasks_TasksFound_Success() {
        String sortBy = "dueDate";
        String direction = "asc";
        String userId = "user123";
        List<TaskItem> sortedTasks = new ArrayList<>();
        sortedTasks.add(new TaskItem());
        when(taskService.sortTasks(sortBy, direction, userId)).thenReturn(sortedTasks);

        ResponseEntity<?> response = taskController.sortTasks(sortBy, direction, userId);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(sortedTasks, response.getBody());
    }

    @Test
    void testSortTasks_NoTasksFound_Failure() {
        String sortBy = "dueDate";
        String direction = "asc";
        String userId = "user123";
        when(taskService.sortTasks(sortBy, direction, userId)).thenReturn(new ArrayList<>());

        ResponseEntity<?> response = taskController.sortTasks(sortBy, direction, userId);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.NO_TASKS_FOUND, response.getBody());
    }

    @Test
    void testSortTasks_Exception_InternalServerError() {
        String sortBy = "dueDate";
        String direction = "asc";
        String userId = "user123";
        when(taskService.sortTasks(sortBy, direction, userId)).thenThrow(RuntimeException.class);

        ResponseEntity<?> response = taskController.sortTasks(sortBy, direction, userId);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.SORT_TASKS_FAILED, response.getBody());
    }

    @Test
    void testGetTask_Exception_InternalServerError() {
        Long taskId = 1L;
        when(taskService.getTask(anyLong())).thenThrow(RuntimeException.class);

        ResponseEntity<?> response = taskController.getTask(taskId);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.GET_TASK_FAILED, response.getBody());
    }

    @Test
    void testDeleteTask_Exception_InternalServerError() {
        Long taskId = 1L;
        when(taskService.getTask(anyLong())).thenReturn(new TaskItem());
        doThrow(RuntimeException.class).when(taskService).deleteTask(anyLong());

        ResponseEntity<?> response = taskController.deleteTask(taskId);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.DELETE_TASK_FAILED, response.getBody());
    }

    @Test
    void testUpdateTask_Exception_InternalServerError() {
        Long taskId = 1L;
        TaskItem taskItem = new TaskItem();
        when(taskService.updateTask(anyLong(), any(TaskItem.class))).thenThrow(RuntimeException.class);

        ResponseEntity<?> response = taskController.updateTask(taskId, taskItem);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.UPDATE_TASK_FAILED, response.getBody());
    }

    @Test
    void testUpdateTask_InternalServerError() {
        Long taskId = 1L;
        when(taskService.updateTask(anyLong())).thenThrow(RuntimeException.class);

        ResponseEntity<?> response = taskController.updateTask(taskId);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.UPDATE_TASK_FAILED, response.getBody());
    }

    @Test
    void testAddTasks_InternalServerError() {
        TaskItems taskItems = new TaskItems();
        when(taskService.addTasks(any())).thenThrow(new RuntimeException("Mocked exception"));

        ResponseEntity<?> response = taskController.addTasks(taskItems);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.ADD_TASKS_FAILED, response.getBody());
    }

    @Test
    void testAddTasks_NoTasksAdded() {
        TaskItems taskItems = new TaskItems();
        when(taskService.addTasks(any())).thenReturn(new ArrayList<>());

        ResponseEntity<?> response = taskController.addTasks(taskItems);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.ADD_TASKS_FAILED, response.getBody());
    }

    @Test
    void testAddTask_InternalServerError() {
        TaskItem taskItem = new TaskItem();
        when(taskService.addTask(any(TaskItem.class))).thenThrow(RuntimeException.class);

        ResponseEntity<?> response = taskController.addTask(taskItem);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.ADD_TASK_FAILED, response.getBody());
    }

    @Test
    void testAddTask_NullReturned() {
        TaskItem taskItem = new TaskItem();
        when(taskService.addTask(any(TaskItem.class))).thenReturn(null);

        ResponseEntity<?> response = taskController.addTask(taskItem);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(AuditConstants.ADD_TASK_FAILED, response.getBody());
    }

    @Test
    void testGetTasks_InternalServerError() {
        when(taskService.getTasks()).thenThrow(RuntimeException.class);

        ResponseEntity<?> response = taskController.getTasks();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(AuditConstants.GET_TASKS_FAILED, response.getBody());
    }
}
