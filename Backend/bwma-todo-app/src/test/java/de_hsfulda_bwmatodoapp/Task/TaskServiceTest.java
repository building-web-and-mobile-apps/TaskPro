package de_hsfulda_bwmatodoapp.Task;

import de.hsfulda.bwmatodoapp.Category.Category;
import de.hsfulda.bwmatodoapp.Category.CategoryRepository;
import de.hsfulda.bwmatodoapp.Priority.Priority;
import de.hsfulda.bwmatodoapp.Priority.PriorityRepository;
import de.hsfulda.bwmatodoapp.Task.TaskItem;
import de.hsfulda.bwmatodoapp.Task.TaskRepository;
import de.hsfulda.bwmatodoapp.Task.TaskService;
import de.hsfulda.bwmatodoapp.Task.TaskSpecification;
import de.hsfulda.bwmatodoapp.User.UserModel;
import de.hsfulda.bwmatodoapp.User.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TaskServiceTest {

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PriorityRepository priorityRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private TaskService taskService;

    TaskServiceTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetTasks() {
        List<TaskItem> tasks = new ArrayList<>();
        when(taskRepository.findAll()).thenReturn(tasks);
        List<TaskItem> result = taskService.getTasks();
        assertEquals(tasks, result);
    }

    @Test
    void testGetTask() {
        TaskItem taskItem = new TaskItem();
        when(taskRepository.findById(1L)).thenReturn(Optional.of(taskItem));
        TaskItem result = taskService.getTask(1L);
        assertEquals(taskItem, result);
    }

    @Test
    void testAddTask() {
        TaskItem taskItem = new TaskItem();
        UserModel user = new UserModel();
        user.setUserId("userId");
        taskItem.setUser(user);
        Category category = new Category();
        category.setId(1L);
        taskItem.setCategory(category);
        Priority priority = new Priority();
        priority.setId(1L);
        taskItem.setPriority(priority);

        when(userRepository.findByUserId(anyString())).thenReturn(Optional.of(user));
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(category));
        when(priorityRepository.findById(anyLong())).thenReturn(Optional.of(priority));
        when(taskRepository.save(taskItem)).thenReturn(taskItem);

        TaskItem result = taskService.addTask(taskItem);

        assertEquals(taskItem, result);
    }

    @Test
    void testAddTasks() {
        TaskItem taskItem1 = new TaskItem();
        TaskItem taskItem2 = new TaskItem();
        TaskItem taskItem3 = new TaskItem();


        UserModel user = new UserModel();
        user.setUserId("userId");
        when(userRepository.findByUserId(anyString())).thenReturn(Optional.of(user));

        Category category = new Category();
        category.setId(1L);
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(category));

        Priority priority = new Priority();
        priority.setId(1L);
        when(priorityRepository.findById(anyLong())).thenReturn(Optional.of(priority));

        taskItem1.setUser(user);
        taskItem1.setPriority(priority);
        taskItem1.setCategory(category);

        taskItem2.setUser(user);
        taskItem2.setPriority(priority);
        taskItem2.setCategory(category);

        taskItem3.setUser(user);
        taskItem3.setPriority(priority);
        taskItem3.setCategory(category);

        List<TaskItem> taskItems = new ArrayList<>();
        taskItems.add(taskItem1);
        taskItems.add(taskItem2);
        taskItems.add(taskItem3);

        when(taskService.addTask(taskItem1)).thenReturn(taskItem1);
        when(taskService.addTask(taskItem2)).thenReturn(taskItem2);
        when(taskService.addTask(taskItem3)).thenReturn(taskItem3);

        List<TaskItem> result = taskService.addTasks(taskItems);

        assertEquals(3, result.size());
        assertEquals(taskItems, result);
    }

    @Test
    void testUpdateTask() {
        Long taskId = 1L;
        TaskItem taskItem = new TaskItem();
        taskItem.setId(taskId);
        taskItem.setDone(false);

        when(taskRepository.findById(anyLong())).thenReturn(Optional.of(taskItem));

        TaskItem updatedTask = taskService.updateTask(taskId);

        assertNotNull(updatedTask);
        assertTrue(updatedTask.isDone());
    }

    @Test
    void testUpdateTask_NotFound() {
        Long taskId = 1L;
        when(taskRepository.findById(anyLong())).thenReturn(Optional.empty());

        TaskItem updatedTask = taskService.updateTask(taskId);

        assertNull(updatedTask);
    }

    @Test
    void testUpdateTask2() {
        Long taskId = 1L;
        TaskItem existingTask = new TaskItem();
        existingTask.setId(taskId);

        TaskItem updatedTaskItem = new TaskItem();
        updatedTaskItem.setTitle("Updated Title");
        updatedTaskItem.setDescription("Updated Description");
        updatedTaskItem.setDone(true);

        UserModel user = new UserModel();
        user.setUserId("userId");
        updatedTaskItem.setUser(user);

        Category category = new Category();
        category.setId(1L);
        updatedTaskItem.setCategory(category);

        Priority priority = new Priority();
        priority.setId(1L);
        updatedTaskItem.setPriority(priority);

        when(taskRepository.findById(anyLong())).thenReturn(Optional.of(existingTask));
        when(userRepository.findByUserId(anyString())).thenReturn(Optional.of(user));
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(category));
        when(priorityRepository.findById(anyLong())).thenReturn(Optional.of(priority));
        when(taskRepository.save(existingTask)).thenReturn(existingTask);

        TaskItem result = taskService.updateTask(taskId, updatedTaskItem);

        assertNotNull(result);
        assertEquals(updatedTaskItem.getTitle(), result.getTitle());
        assertEquals(updatedTaskItem.getDescription(), result.getDescription());
        assertEquals(updatedTaskItem.isDone(), result.isDone());
        assertEquals(updatedTaskItem.getUser(), result.getUser());
        assertEquals(updatedTaskItem.getCategory(), result.getCategory());
        assertEquals(updatedTaskItem.getPriority(), result.getPriority());
    }

    @Test
    void testUpdateTask2_TaskNotFound() {
        Long taskId = 1L;
        when(taskRepository.findById(anyLong())).thenReturn(Optional.empty());

        TaskItem result = taskService.updateTask(taskId, new TaskItem());

        assertNull(result);
    }

    @Test
    void testUpdateTask2_InvalidUser() {
        Long taskId = 1L;
        TaskItem existingTask = new TaskItem();
        existingTask.setId(taskId);

        TaskItem updatedTaskItem = new TaskItem();
        UserModel user = new UserModel();
        user.setUserId("nonExistingUserId");
        updatedTaskItem.setUser(user);

        when(taskRepository.findById(anyLong())).thenReturn(Optional.of(existingTask));
        when(userRepository.findByUserId(anyString())).thenReturn(Optional.empty());

        TaskItem result = taskService.updateTask(taskId, updatedTaskItem);

        assertNull(result);
    }

    @Test
    void testUpdateTask2_InvalidCategory() {
        Long taskId = 1L;
        TaskItem existingTask = new TaskItem();
        existingTask.setId(taskId);

        TaskItem updatedTaskItem = new TaskItem();
        Category category = new Category();
        category.setId(999L);
        updatedTaskItem.setCategory(category);

        UserModel user = new UserModel();
        user.setUserId("nonExistingUserId");
        updatedTaskItem.setUser(user);

        when(taskRepository.findById(anyLong())).thenReturn(Optional.of(existingTask));
        when(userRepository.findByUserId(anyString())).thenReturn(Optional.of(user));
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.empty());

        TaskItem result = taskService.updateTask(taskId, updatedTaskItem);

        assertNull(result);
    }

    @Test
    void testUpdateTask_InvalidPriority() {
        Long taskId = 1L;
        TaskItem existingTask = new TaskItem();
        existingTask.setId(taskId);

        TaskItem updatedTaskItem = new TaskItem();
        Priority priority = new Priority();
        priority.setId(999L);
        updatedTaskItem.setPriority(priority);

        UserModel user = new UserModel();
        user.setUserId("nonExistingUserId");
        updatedTaskItem.setUser(user);

        Category category = new Category();
        category.setId(1L);
        updatedTaskItem.setCategory(category);

        when(taskRepository.findById(anyLong())).thenReturn(Optional.of(existingTask));
        when(userRepository.findByUserId(anyString())).thenReturn(Optional.of(user));
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.of(category));
        when(priorityRepository.findById(anyLong())).thenReturn(Optional.empty());

        TaskItem result = taskService.updateTask(taskId, updatedTaskItem);

        assertNull(result);
    }

    @Test
    void testDeleteTask() {
        Long taskId = 1L;
        taskService.deleteTask(taskId);
        verify(taskRepository).deleteById(taskId);
    }

    @Test
    void testSearchTasks() {
        String keyword = "Grocery";
        String userId = "userId";

        TaskItem task1 = new TaskItem();
        task1.setId(1L);
        task1.setTitle("Grocery Shopping");
        UserModel user1 = new UserModel();
        user1.setUserId(userId);
        task1.setUser(user1);

        TaskItem task2 = new TaskItem();
        task2.setId(2L);
        task2.setTitle("Work Task");
        UserModel user2 = new UserModel();
        user2.setUserId(userId);
        task1.setUser(user2);

        List<TaskItem> mockTasks = new ArrayList<>();
        mockTasks.add(task1);
        mockTasks.add(task2);

        when(taskRepository.findByKeyword(anyString())).thenReturn(mockTasks);

        List<TaskItem> result = taskService.searchTasks(keyword, userId);

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals(task1, result.get(0));
    }

    @Test
    void testSearchTasks_NoTasksFound() {
        String keyword = "Nonexistent";
        String userId = "userId";

        when(taskRepository.findByKeyword(anyString())).thenReturn(new ArrayList<>());

        List<TaskItem> result = taskService.searchTasks(keyword, userId);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void testFilterTasks_NoTasksFound() {
        boolean done = true;
        Long priorityId = 1L;
        LocalDate dueDate = LocalDate.now();
        String userId = "userId";

        Specification<TaskItem> spec = Specification.where(TaskSpecification.isDone(done))
                .and(TaskSpecification.isDueBy(dueDate));

        when(priorityRepository.findById(anyLong())).thenReturn(Optional.of(new Priority()));
        when(taskRepository.findAll(spec)).thenReturn(new ArrayList<>());

        List<TaskItem> result = taskService.filterTasks(done, priorityId, dueDate, userId);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void testFilterTasks_InvalidPriority() {
        Boolean done = true;
        Long priorityId = 999L;
        LocalDate dueDate = LocalDate.now();
        String userId = "userId";

        when(priorityRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(RuntimeException.class, () -> taskService.filterTasks(done, priorityId, dueDate, userId));
    }

    @Test
    void testSortTasks() {
        String sortBy = "dueDate";
        String direction = "asc";
        String userId = "userId";

        TaskItem task1 = new TaskItem();
        task1.setId(1L);
        task1.setTitle("Task 1");
        UserModel user1 = new UserModel();
        user1.setUserId(userId);
        task1.setUser(user1);
        task1.setDueDate(LocalDateTime.now().plusDays(1));

        TaskItem task2 = new TaskItem();
        task2.setId(2L);
        task2.setTitle("Task 2");
        UserModel user2 = new UserModel();
        user2.setUserId(userId);
        task2.setUser(user2);
        task2.setDueDate(LocalDateTime.now());

        TaskItem task3 = new TaskItem();
        task3.setId(3L);
        task3.setTitle("Task 3");
        UserModel user3 = new UserModel();
        user3.setUserId(userId);
        task3.setUser(user3);
        task3.setDueDate(LocalDateTime.now().minusDays(1));

        List<TaskItem> mockTasks = new ArrayList<>();
        mockTasks.add(task1);
        mockTasks.add(task2);
        mockTasks.add(task3);

        when(taskRepository.findAll(any(Sort.class))).thenReturn(mockTasks);

        List<TaskItem> result = taskService.sortTasks(sortBy, direction, userId);

        assertNotNull(result);
        assertEquals(3, result.size());
        assertTrue(result.indexOf(task2) < result.indexOf(task3));
        assertTrue(result.indexOf(task3) > result.indexOf(task1));
    }

    @Test
    void testSortTasks_NoTasksFound() {
        String sortBy = "dueDate";
        String direction = "asc";
        String userId = "userId";

        when(taskRepository.findAll(any(Sort.class))).thenReturn(new ArrayList<>());

        List<TaskItem> result = taskService.sortTasks(sortBy, direction, userId);

        assertNotNull(result);
        assertTrue(result.isEmpty());
    }

    @Test
    void testGetTask_NotFound() {
        Long taskId = 1L;
        when(taskRepository.findById(taskId)).thenReturn(Optional.empty());

        TaskItem result = taskService.getTask(taskId);

        assertNull(result);
    }

    @Test
    void testAddTask_NullCase() {
        TaskItem taskItem = new TaskItem();
        UserModel user = new UserModel();
        user.setUserId("123");
        taskItem.setUser(user);
        Category category = new Category();
        category.setId(1L);
        taskItem.setCategory(category);
        Priority priority = new Priority();
        priority.setId(1L);
        taskItem.setPriority(priority);

        taskItem.setCategory(category);
        taskItem.setPriority(priority);

        when(userRepository.findByUserId(anyString())).thenReturn(Optional.empty());
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.empty());
        when(priorityRepository.findById(anyLong())).thenReturn(Optional.empty());

        TaskItem result = taskService.addTask(taskItem);

        assertNull(result);
    }
}
