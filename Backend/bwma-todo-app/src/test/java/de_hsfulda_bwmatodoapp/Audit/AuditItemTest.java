package de_hsfulda_bwmatodoapp.Audit;

import de.hsfulda.bwmatodoapp.Audit.AuditItem;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.time.LocalDateTime;

class AuditItemTest {

    @Test
    void testAuditItemConstruction() {
        AuditItem auditItem = new AuditItem();
        assertNotNull(auditItem);
    }

    @Test
    void testAuditItemGettersAndSetters() {
        AuditItem auditItem = new AuditItem();

        Long id = 1L;
        auditItem.setId(id);
        assertEquals(id, auditItem.getId());

        String url = "http://example.com";
        auditItem.setUrl(url);
        assertEquals(url, auditItem.getUrl());

        String uri = "/example";
        auditItem.setUri(uri);
        assertEquals(uri, auditItem.getUri());

        String method = "GET";
        auditItem.setMethod(method);
        assertEquals(method, auditItem.getMethod());

        String parameters = "param1=value1&param2=value2";
        auditItem.setParameters(parameters);
        assertEquals(parameters, auditItem.getParameters());

        LocalDateTime startTime = LocalDateTime.now();
        auditItem.setStart_time(startTime);
        assertEquals(startTime, auditItem.getStart_time());

        String user = "testUser";
        auditItem.setUser(user);
        assertEquals(user, auditItem.getUser());
    }
}
