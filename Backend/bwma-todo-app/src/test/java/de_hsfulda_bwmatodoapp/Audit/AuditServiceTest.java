package de_hsfulda_bwmatodoapp.Audit;

import de.hsfulda.bwmatodoapp.Audit.AuditItem;
import de.hsfulda.bwmatodoapp.Audit.AuditRepository;
import de.hsfulda.bwmatodoapp.Audit.AuditService;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Vector;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

class AuditServiceTest {

    @Mock
    private AuditRepository auditRepository;

    private AuditService auditService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        auditService = new AuditService(auditRepository);
    }

    @Test
    void testLog() {
        String username = "testUser";
        String activity = "testActivity";
        auditService.log(username, activity);
    }

    @Test
    void testAudit() {
        AuditItem auditItem = new AuditItem();
        auditService.audit(auditItem);
        verify(auditRepository, times(1)).save(auditItem);
    }

    @Test
    void testAuditRequestNoParameters() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://example.com"));
        when(request.getRequestURI()).thenReturn("/example");
        when(request.getMethod()).thenReturn("GET");
        when(request.getParameterNames()).thenReturn(new Vector<String>().elements());

        auditService.auditRequest(request);

        verify(request, never()).getParameter(anyString());
    }

    @Test
    void testAuditRequestParameters() {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://example.com"));
        when(request.getRequestURI()).thenReturn("/example");
        when(request.getMethod()).thenReturn("GET");
        Vector<String> parameterNames = new Vector<>();
        parameterNames.add("param1");
        parameterNames.add("param2");
        when(request.getParameterNames()).thenReturn(parameterNames.elements());

        auditService.auditRequest(request);

        assertAll(
            () -> verify(request, times(1)).getParameter("param1"),
            () -> verify(request, times(1)).getParameter("param2")
        );
    }
}
