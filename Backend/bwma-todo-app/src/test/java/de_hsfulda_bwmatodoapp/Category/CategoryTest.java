package de_hsfulda_bwmatodoapp.Category;

import de.hsfulda.bwmatodoapp.Category.Category;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CategoryTest {

    @Test
    void testNoArgsConstructor() {
        // Arrange
        Category category = new Category();

        // Act
        Long id = category.getId();
        String name = category.getName();

        // Assert
        assertNull(id);
        assertNull(name);
    }

    @Test
    void testAllArgsConstructor() {
        Long id = 1L;
        String name = "TestCategory";
        Category category = new Category(id, name);

        Long categoryId = category.getId();
        String categoryName = category.getName();

        assertEquals(id, categoryId);
        assertEquals(name, categoryName);
    }

    @Test
    void testGetterSetter() {
        Long id = 1L;
        String name = "TestCategory";
        Category category = new Category();

        category.setId(id);
        category.setName(name);

        assertEquals(id, category.getId());
        assertEquals(name, category.getName());
    }

    @Test
    void testToString() {
        Long id = 1L;
        String name = "TestCategory";
        Category category = new Category(id, name);

        String stringRepresentation = category.toString();

        assertEquals("Category(id=1, name=TestCategory)", stringRepresentation);
    }

    @Test
    void testEqualsAndHashCode() {
        Category category1 = new Category(1L, "TestCategory");
        Category category2 = new Category(1L, "TestCategory");

        boolean equals = category1.equals(category2);
        int hashCode1 = category1.hashCode();
        int hashCode2 = category2.hashCode();

        assertTrue(equals);
        assertEquals(hashCode1, hashCode2);
    }
}
