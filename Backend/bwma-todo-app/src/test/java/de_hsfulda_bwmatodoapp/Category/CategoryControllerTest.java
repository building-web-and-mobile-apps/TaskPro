package de_hsfulda_bwmatodoapp.Category;

import de.hsfulda.bwmatodoapp.Category.Category;
import de.hsfulda.bwmatodoapp.Category.CategoryController;
import de.hsfulda.bwmatodoapp.Category.CategoryRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class CategoryControllerTest {

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private CategoryController categoryController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetAllCategories() {
        // Arrange
        List<Category> categories = Arrays.asList(
                new Category(1L, "Category 1"),
                new Category(2L, "Category 2")
        );
        when(categoryRepository.findAll()).thenReturn(categories);

        // Act
        List<Category> result = categoryController.getAllCategories();

        // Assert
        assertEquals(categories.size(), result.size());
        assertEquals(categories.get(0).getName(), result.get(0).getName());
        assertEquals(categories.get(1).getName(), result.get(1).getName());

        verify(categoryRepository, times(1)).findAll();
    }

    @Test
    void testCreateCategory() {
        // Arrange
        Category category = new Category(1L, "Test Category");
        when(categoryRepository.save(any(Category.class))).thenReturn(category);

        // Act
        Category result = categoryController.createCategory(category);

        // Assert
        assertEquals(category.getId(), result.getId());
        assertEquals(category.getName(), result.getName());

        verify(categoryRepository, times(1)).save(any(Category.class));
    }
}
