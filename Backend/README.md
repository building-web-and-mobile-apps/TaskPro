# Backend

Welcome to the TaskPro backend repository! This repository contains the source code for the backend API built using Spring Boot and managed with Gradle. This backend API provides a set of RESTful endpoints for handling various functionalities required by the application.
## Getting Started

To get started with the backend API, follow these steps: 

1. Clone this repository to your local machine:

```bash
  git clone https://gitlab.cs.hs-fulda.de/fdai6557/bwma-project-todos.git
  cd bwma-project-todos/Backend/bwma-todo-app
```

2. Open the project in your preferred IDE.

3. Build the project using Gradle:
```bash
gradle build
```

4. Run the application:
```bash
gradle bootRun
```

The application will start running on http://localhost:8080 by default.

## Configuration

You can configure the application properties in the `application.properties` or `application.yml` file located in the `src/main/resources` directory.

## Project Structure

The project follows a standard Spring Boot project structure. Here's a brief overview:

* `src/main/java`: This directory contains all the Java source code files of the project.
    * `de.hsfulda.bwmatodoapp`: This is the base package of the project.
        * `User`: Contains classes related to user management.
        * `Task`: Contains classes related to tasks management.
        * `Category`: Contains classes related to task categories.
        * `Priority`: Contains classes related to task priorities.
        * `Weather`: Contains classes related to weather functionality.
        * `Audit`: Contains classes related to auditing user activities.
        * `Auth`: Contains classes related to Firebase authentication and authorization.
        * `Config`: Contains configuration classes for Spring Boot, Firebase and MySQL.
* `src/main/resources`: This directory contains non-Java resources such as properties files, XML configuration files, etc.
* `src/test`: This directory contains test classes and resources for testing the project code.

## User Endpoints

### Add User

-   Description: Add a new user to the system.
-   Request Type: POST
-   Endpoint Route: `/users`
-   Parameters:
    -   `user`: Request body containing user details in JSON format.
-   Response:
    -   If successful, returns the created user with HTTP status code 201 (Created).
    -   If unsuccessful, returns an error message with HTTP status code 500 (Internal Server Error).

### Update User

-   Description: Update an existing user in the system.
-   Request Type: PUT
-   Endpoint Route: `/users/{userId}`
-   Parameters:
    -   `userId`: Path variable representing the ID of the user to be updated.
    -   `userDetails`: Request body containing updated user details in JSON format.
-   Response:
    -   If successful, returns the updated user with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 500 (Internal Server Error).

### Get User By User ID

-   Description: Retrieve user details by user ID.
-   Request Type: GET
-   Endpoint Route: `/users`
-   Parameters:
    -   `userId`: Request parameter representing the ID of the user to be retrieved.
-   Response:
    -   If successful, returns the user details with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 500 (Internal Server Error).

## Task Endpoints

### Get Tasks

-   Description: Retrieve all tasks.
-   Request Type: GET
-   Endpoint Route: `/tasks`
-   Parameters: None
-   Response:
    -   If successful, returns a list of tasks with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 500 (Internal Server Error).

### Add Task

-   Description: Add a new task to the system.
-   Request Type: POST
-   Endpoint Route: `/tasks`
-   Parameters:
    -   `taskItem`: Request body containing task details in JSON format.
-   Response:
    -   If successful, returns the created task with HTTP status code 201 (Created).
    -   If unsuccessful, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Add Tasks (Bulk)

-   Description: Add multiple tasks to the system.
-   Request Type: POST
-   Endpoint Route: `/tasks/bulk`
-   Parameters:
    -   `taskItems`: Request body containing a list of task details in JSON format.
-   Response:
    -   If successful, returns the list of created tasks with HTTP status code 201 (Created).
    -   If unsuccessful, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Update Task Status

-   Description: Update the status of a task.
-   Request Type: PUT
-   Endpoint Route: `/tasks/status/{id}`
-   Parameters:
    -   `id`: Path variable representing the ID of the task to be updated.
-   Response:
    -   If successful, returns the updated task with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Update Task

-   Description: Update an existing task in the system.
-   Request Type: PUT
-   Endpoint Route: `/tasks/{id}`
-   Parameters:
    -   `id`: Path variable representing the ID of the task to be updated.
    -   `taskItem`: Request body containing updated task details in JSON format.
-   Response:
    -   If successful, returns the updated task with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Delete Task

-   Description: Delete a task from the system.
-   Request Type: DELETE
-   Endpoint Route: `/tasks/{id}`
-   Parameters:
    -   `id`: Path variable representing the ID of the task to be deleted.
-   Response:
    -   If successful, returns a success message with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Get Task By ID

-   Description: Retrieve a task by its ID.
-   Request Type: GET
-   Endpoint Route: `/tasks/{id}`
-   Parameters:
    -   `id`: Path variable representing the ID of the task to be retrieved.
-   Response:
    -   If successful, returns the task details with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Search Tasks

-   Description: Search for tasks by keyword.
-   Request Type: GET
-   Endpoint Route: `/tasks/search`
-   Parameters:
    -   `keyword`: Request parameter representing the keyword to search for.
    -   `userId`: Request parameter representing the ID of the user.
-   Response:
    -   If successful, returns a list of tasks matching the search criteria with HTTP status code 200 (OK).
    -   If no tasks are found, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Filter Tasks

-   Description: Filter tasks based on status, priority, and due date.
-   Request Type: GET
-   Endpoint Route: `/tasks/filter`
-   Parameters:
    -   `done`: Optional request parameter representing the status of the tasks (true/false).
    -   `priorityId`: Optional request parameter representing the ID of the priority.
    -   `dueDate`: Optional request parameter representing the due date of the tasks.
    -   `userId`: Request parameter representing the ID of the user.
-   Response:
    -   If successful, returns a list of filtered tasks with HTTP status code 200 (OK).
    -   If no tasks are found, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Sort Tasks

-   Description: Sort tasks by a specified field and direction.
-   Request Type: GET
-   Endpoint Route: `/tasks/sort`
-   Parameters:
    -   `sortBy`: Request parameter representing the field to sort by.
    -   `direction`: Optional request parameter representing the sorting direction (asc/desc).
    -   `userId`: Request parameter representing the ID of the user.
-   Response:
    -   If successful, returns a list of sorted tasks with HTTP status code 200 (OK).
    -   If no tasks are found, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Upload Multiple Files to Task

-   Description: Upload multiple files as attachments to a task.
-   Request Type: POST
-   Endpoint Route: `/tasks/{taskId}/attachments`
-   Parameters:
    -   `files`: Request parameter representing the list of files to upload.
    -   `taskId`: Path variable representing the ID of the task.
-   Response:
    -   If successful, returns a success message with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 400 (Bad Request) or 500 (Internal Server Error).

### Get Attachments By Task ID

-   Description: Retrieve attachments associated with a task by its ID.
-   Request Type: GET
-   Endpoint Route: `/tasks/{taskId}/attachments`
-   Parameters:
    -   `taskId`: Path variable representing the ID of the task.
-   Response:
    -   If attachments are found, returns the list of attachments with HTTP status code 200 (OK).
    -   If no attachments are found, returns HTTP status code 204 (No Content) or 404 (Not Found) or 500 (Internal Server Error).

### Delete Attachment

-   Description: Delete an attachment associated with a task.
-   Request Type: DELETE
-   Endpoint Route: `/tasks/{taskId}/attachments/{attachmentId}`
-   Parameters:
    -   `taskId`: Path variable representing the ID of the task.
    -   `attachmentId`: Path variable representing the ID of the attachment to delete.
-   Response:
    -   If successful, returns a success message with HTTP status code 200 (OK).
    -   If unsuccessful, returns an error message with HTTP status code 400 (Bad Request) or 404 (Not Found) or 500 (Internal Server Error).

## Category Endpoints

### Get All Categories

-   Description: Retrieve all categories.
-   Request Type: GET
-   Endpoint Route: `/categories`
-   Parameters: None
-   Response:
    -   Returns a list of all categories with HTTP status code 200 (OK).

### Create Category

-   Description: Create a new category.
-   Request Type: POST
-   Endpoint Route: `/categories`
-   Parameters:
    -   `category`: Request body containing category details in JSON format.
-   Response:
    -   Returns the created category with HTTP status code 200 (OK).

## Priority Endpoints

### Get All Priorities

-   Description: Retrieve all priorities.
-   Request Type: GET
-   Endpoint Route: `/priorities`
-   Parameters: None
-   Response:
    -   Returns a list of all priorities with HTTP status code 200 (OK).

## Weather Endpoints

### Get Weather Data for City

-   Description: Retrieve weather data for a specific city.
-   Request Type: GET
-   Endpoint Route: `/weather/{city}`
-   Parameters:
    -   `city`: Path variable representing the name of the city.
-   Response:
    -   If successful, returns weather data for the specified city with HTTP status code 200 (OK).
    -   If the city is not found, returns an error message with HTTP status code 404 (Not Found).
    -   If an error occurs, returns an error message with HTTP status code 500 (Internal Server Error).

## Request models

### UserModel

-   Description: Represents a user in the system.
-   Fields:
    -   `id` (Long): Unique identifier for the user. Auto-generated.
    -   `email` (String): Email address of the user.
    -   `name` (String): Name of the user.
    -   `userId` (String): Unique identifier for the user, typically used for authentication purposes.
    -   `createdAt` (LocalDateTime): Date and time when the user was created.
    -   `updatedAt` (LocalDateTime): Date and time when the user was last updated.
- Example Request Body:
    ```json
    {
        "name": "Test",
        "email": "test@test.de",
        "userId": "12345",
        "createdAt": "2024-02-20T00:00:00.000",
        "updatedAt": "2024-02-20T00:00:00.000"
    }
    ```

### TaskItem

-   Description: Represents a task in the system.
-   Fields:
    -   `id` (Long): Unique identifier for the task. Auto-generated.
    -   `title` (String): Title of the task.
    -   `description` (String): Description of the task.
    -   `done` (boolean): Flag indicating whether the task is completed.
    -   `dueDate` (LocalDateTime): Date and time when the task is due.
    -   `completeDate` (LocalDateTime): Date and time when the task was completed.
    -   `archived` (boolean): Flag indicating whether the task is archived.
    -   `user` (UserModel): The user assigned to the task.
    -   `category` (Category): The category to which the task belongs.
    -   `priority` (Priority): The priority level of the task.
    -   `attachments` (List<FileAttachment>): List of file attachments associated with the task.
- Example Request Body:
    ```json
    {
        "title": "Test Task",
        "description": "Task for testing",
        "done": false,
        "dueDate": "2024-03-31T00:00:00.000",
        "completeDate": "2024-03-31T00:00:00.000",
        "archived": false,
        "user": {
            "userId": "12345"
        },
        "priority": {
            "id": 1
        },
        "category": {
            "id": 3
        }
    }
    ```

### TaskItems

-   Description: Represents a collection of task items.
-   Fields:
    -   `taskItems` (List<TaskItem>): List of task items.

### Category

-   Description: Represents a category in the system.
-   Fields:
    -   `id` (Long): Unique identifier for the category. Auto-generated.
    -   `name` (String): Name of the category.
- Example Request Body:
    ```json
    {
        "name": "Test"
    }
    ```

### Priority
- Description: Represents a priority in the system.
- Fields:
    -   `id` (Long): Unique identifier for the priority. Auto-generated.
    -   `name` (String): Name of the priority.

### FileAttachment

-   Description: Represents a file attachment associated with a task in the system.
-   Fields:
    -   `id` (Long): Unique identifier for the file attachment. Auto-generated.
    -   `fileName` (String): Name of the attached file.
    -   `fileType` (String): Type of the attached file.
    -   `size` (long): Size of the attached file.
    -   `task` (TaskItem): Task to which the file attachment is associated.
    -   `filePath` (String): Path of the attached file.
- Example Request Body (multipart/form-data):
    ```json
    {
        "files": <Choose files here>
    }
    ```