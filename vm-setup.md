## Setting up and running TaskPro on a virtual machine

This guide will walk you through the steps to set up and run TaskPro on a virtual machine.

### Step 1: Update Package Index

Before installing Docker, it's recommended to update the package index to ensure you have the latest version available.

```bash

sudo apt update

```

### Step 2: Install Dependencies

Ensure that you have the necessary packages to allow apt to use a repository over HTTPS:

```bash

sudo apt install apt-transport-https ca-certificates curl software-properties-common

```

### Step 3: Add Docker Repository GPG Key

Add Docker's official GPG key to ensure the authenticity of the software package:

```bash

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

```

### Step 4: Add Docker Repository

Add the Docker repository to your system's software repository list:

```bash

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

```

### Step 5: Install Docker Engine

Update the package index again, then install Docker:

```bash

sudo apt update

sudo apt install docker-ce

```

### Step 6: Verify Docker Installation

Check that Docker has been installed correctly by running the following command:

```bash

sudo systemctl status docker

```

This command will show you the status of the Docker service.

### Step 7: Run Docker Without Sudo (Optional)

By default, the Docker command must be run with sudo. If you want to run Docker commands without sudo, add your user to the docker group:

```bash

sudo usermod -aG docker ${USER}

```

Log out and log back in for the changes to take effect.

### Step 8: Verify Docker Installation as Non-Root User (Optional)

After adding your user to the docker group, you can verify that you can run Docker commands without sudo:

```bash

docker run hello-world

```

This command should download a test image and run it in a container. If everything is configured correctly, you'll see a message indicating that your Docker installation is working properly.

### Step 9: Verify Installation of Docker Compose

Before using Docker Compose, it's a good idea to verify that the installation was successful by running:

```bash

docker compose version

```

This command should display the installed version of Docker Compose.

### Step 10: Create a Docker Compose File

Docker Compose uses a YAML file (`docker-compose.yml`) to define the services, networks, and volumes for your application. Create a new directory for the project and navigate into it:

```bash

mkdir task_pro

cd task_pro

```

Create a `docker-compose.yml` file and copy the contents of the TaskPro Docker Compose file found [here](docker-compose.yml) to it.


### Step 11: Run Docker Compose

Once you have created your `docker-compose.yml` file, you can use Docker Compose to start your application:

```bash

docker compose up

```

This command will build the necessary Docker images and start the containers defined in your `docker-compose.yml` file.

Warning: The initial docker compose might fail since the MySQL service needs a moment to start. If this is the case, please stop and remove containers (using the command in the next step) and run Docker Compose once again.

You might need to go into the mysql container and create the todoDB using this SQL command:

```sql

CREATE DATABASE todoDB;

```

After that run the docker compose up command again

### Step 12: Stop and Remove Containers

To stop the containers started by Docker Compose, you can use the following command:

```bash

docker compose down

```

This command will stop and remove the containers defined in your `docker-compose.yml` file.

### Step 13: Access the running application

Using a browser, access the running application at `localhost:80`
