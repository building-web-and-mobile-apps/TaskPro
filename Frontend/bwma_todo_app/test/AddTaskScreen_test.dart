import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:bwma_todo_app/screens/AddTaskScreen.dart';
import 'package:bwma_todo_app/core/TaskFutures.dart';

class MockTaskFutures extends Mock implements TaskFutures {}

void main() {
  group('AddTaskScreen Widget Tests', () {
    testWidgets('Renders AddTaskScreen', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: AddTaskScreen(taskFutures: MockTaskFutures()),
        ),
      );

      expect(find.byType(AddTaskScreen), findsOneWidget);
    });

    testWidgets('Form validation shows error on empty submission',
        (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: AddTaskScreen(taskFutures: MockTaskFutures()),
        ),
      );

      // Scroll the widget into view
      await tester.ensureVisible(find.byKey(const ValueKey('saveButton')));

      await tester.tap(find.text('Save'));
      await tester.pumpAndSettle();

      expect(find.text('Please enter the task name'), findsOneWidget);
      expect(find.text('Please enter the task description'), findsOneWidget);
      expect(find.text('Please enter the due date'), findsOneWidget);
      expect(find.text('Please enter the priority'), findsOneWidget);
      expect(find.text('Please enter the category'), findsOneWidget);
    });
  });
}
