import 'package:bwma_todo_app/core/TaskFutures.dart';
import 'package:bwma_todo_app/model/Category.dart';
import 'package:bwma_todo_app/model/Priority.dart';
import 'package:bwma_todo_app/model/Task.dart';
import 'package:bwma_todo_app/model/UserDTO.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('TaskFutures Integration Tests', () {
    late TaskFutures taskFutures;
    // Insert your token here + make sure the backend is running
    const String token =
        'eyJhbGciOiJSUzI1NiIsImtpZCI6ImJhNjI1OTZmNTJmNTJlZDQ0MDQ5Mzk2YmU3ZGYzNGQyYzY0ZjQ1M2UiLCJ0eXAiOiJKV1QifQ.eyJhZG1pbiI6dHJ1ZSwiaXNzIjoiaHR0cHM6Ly9zZWN1cmV0b2tlbi5nb29nbGUuY29tL2J3bWEtdG9kbyIsImF1ZCI6ImJ3bWEtdG9kbyIsImF1dGhfdGltZSI6MTcxMTM3Mzc2NCwidXNlcl9pZCI6InhQTzJ5M0lVSUdocVJXRTJUZDd4ejVQWnlZWjIiLCJzdWIiOiJ4UE8yeTNJVUlHaHFSV0UyVGQ3eHo1UFp5WVoyIiwiaWF0IjoxNzExMzczNzY0LCJleHAiOjE3MTEzNzczNjQsImVtYWlsIjoiYndtYS1kZXZAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbImJ3bWEtZGV2QGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.cxjWIYspz0D3jUof5X7xFaDSwNxs6nxR1R-_J9CltNlwyoXxOUu6g9sAeeMh94vtLrI2sLAkFNR4k56c1u_LGGtNX-bGLAx8LhCBix3weNYS2QWzCqYWfLYm-OquHnqfrVT67vnWqgQE94bGrs8vHy5ItAacEC5qC2zzwz-pdRzoz9n1hpZ4FMhoKPvv-MkPP2cEzM5gt_1WC3L7oMjw1OU0IM9qrcAVP_B6EtnhLZqIRrv7sNbT2t7333m4ZWilKVn5QvHuXvU_3XbHBRR6M63i7mkw6iRGFn2aZ3CEkoTgragAn30zBHM_DiLVLE-2gmsjVlogfP0dHzvNp_F_Hg';
    setUp(() {
      taskFutures = TaskFutures(token);
    });

    test('fetchTasks should return a list of tasks', () async {
      final List<TaskModel> result = await taskFutures.fetchTasks();
      expect(result, isA<List<TaskModel>>());
    });

    test('fetchTaskById should return a task', () async {
      final CreateCategory createCategory = CreateCategory(id: 1);
      final CreatePriority createPriority = CreatePriority(id: 1);
      final createUser = CreateUser(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
      );

      final CreateTask taskToAdd = CreateTask(
        taskId: 1,
        name: 'Test Task Add3',
        description: 'Test Description',
        dueDate: DateTime.now(),
        done: false,
        priority: createPriority,
        category: createCategory,
        completedDate: null,
        archived: false,
        user: createUser,
      );

      await taskFutures.addTask(taskToAdd);

      final List<TaskModel> tasksAfterUpdate = await taskFutures.fetchTasks();

      final priority = Priority(id: 1, name: 'High');
      final category = Category(id: 1, name: 'Work');
      final user = UserDTO(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
        id: 1,
        email: 'test@test.de',
        name: 'Test',
        createdAt: DateTime.now(),
        updatedAt: DateTime.now(),
        role: 'user',
      );

      final addedTask = tasksAfterUpdate.firstWhere(
        (task) => task.name == taskToAdd.name,
        orElse: () => TaskModel(
          taskId: -1,
          name: 'WRONG',
          description: 'WRONG',
          dueDate: DateTime.now(),
          done: false,
          priority: priority,
          category: category,
          completedDate: null,
          archived: false,
          user: user,
        ),
      );

      final TaskModel? result = await taskFutures.fetchTaskById(addedTask.taskId);
      expect(result, isA<TaskModel>());

      await taskFutures.deleteTask(addedTask.taskId);
      final TaskModel? tasksAfterDeleting =
          await taskFutures.fetchTaskById(addedTask.taskId);
      expect(tasksAfterDeleting, isNull,
          reason: 'Task should be deleted successfully');
    });

    DateTime now = DateTime.now();

    test('addTask should print success message on success', () async {
      final CreateCategory createCategory = CreateCategory(id: 1);
      final CreatePriority createPriority = CreatePriority(id: 1);
      final createUser = CreateUser(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
      );

      final CreateTask taskToAdd = CreateTask(
        taskId: 1,
        name: 'Test Task Add',
        description: 'Test Description',
        dueDate: now,
        done: false,
        priority: createPriority,
        category: createCategory,
        completedDate: null,
        archived: false,
        user: createUser,
      );

      await taskFutures.addTask(taskToAdd);

      final List<TaskModel> tasksAfterAdding = await taskFutures.fetchTasks();

      final priority = Priority(id: 1, name: 'High');
      final category = Category(id: 1, name: 'Work');
      final user = UserDTO(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
        id: 1,
        email: 'test@test.de',
        name: 'Test',
        createdAt: DateTime.now(),
        updatedAt: DateTime.now(),
        role: 'user',

      );

      final matchingTask = tasksAfterAdding.firstWhere(
        (task) => task.name == taskToAdd.name,
        orElse: () => TaskModel(
          taskId: -1,
          name: 'WRONG',
          description: 'WRONG',
          dueDate: now,
          done: false,
          priority: priority,
          category: category,
          completedDate: null,
          archived: false,
          user: user,
        ),
      );

      CreatePriority expectedPriority = CreatePriority(id: 1);
      CreateCategory expectedCategory = CreateCategory(id: 1);
      CreateUser expectedUser =
          CreateUser(userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22');

      expect(matchingTask, isNotNull,
          reason: 'Task with the same name should exist in the list');
      expect(matchingTask.name, equals(taskToAdd.name));
      expect(matchingTask.done, equals(taskToAdd.done));
      expect(matchingTask.archived, equals(taskToAdd.archived));
      expect(expectedPriority.id, equals(taskToAdd.priority.id));
      expect(expectedCategory.id, equals(taskToAdd.category.id));
      expect(matchingTask.description, equals(taskToAdd.description));
      expect(matchingTask.completedDate, equals(taskToAdd.completedDate));
      expect(expectedUser.userId, equals(taskToAdd.user.userId));

      await taskFutures.deleteTask(matchingTask.taskId);

      final TaskModel? tasksAfterDeleting =
          await taskFutures.fetchTaskById(matchingTask.taskId);
      expect(tasksAfterDeleting, isNull,
          reason: 'Task should be deleted successfully');
    });

    test('updateTask should update an existing task', () async {
      final CreateCategory createCategory = CreateCategory(id: 1);
      final CreatePriority createPriority = CreatePriority(id: 1);
      final createUser = CreateUser(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
      );

      final CreateTask taskToUpdate = CreateTask(
        taskId: 1,
        name: 'Test Task Update x',
        description: 'Test Description',
        dueDate: now,
        done: false,
        priority: createPriority,
        category: createCategory,
        completedDate: null,
        archived: false,
        user: createUser,
      );

      await taskFutures.addTask(taskToUpdate);

      await taskFutures.updateTask(taskToUpdate);

      final List<TaskModel> tasksAfterUpdate = await taskFutures.fetchTasks();

      final priority = Priority(id: 1, name: 'High');
      final category = Category(id: 1, name: 'Work');
      final user = UserDTO(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
        id: 1,
        email: 'test@test.de',
        name: 'Test',
        createdAt: DateTime.now(),
        updatedAt: DateTime.now(),
        role: 'user',

      );

      final updatedTask = tasksAfterUpdate.firstWhere(
        (task) => task.name == taskToUpdate.name,
        orElse: () => TaskModel(
          taskId: -1,
          name: 'WRONG',
          description: 'WRONG',
          dueDate: now,
          done: false,
          priority: priority,
          category: category,
          completedDate: null,
          archived: false,
          user: user,
        ),
      );

      expect(updatedTask, isNotNull,
          reason: 'Updated task should exist in the list');
      expect(updatedTask.name, equals(taskToUpdate.name));
      expect(updatedTask.completedDate, equals(taskToUpdate.completedDate));
      expect(updatedTask.description, equals(taskToUpdate.description));
      expect(updatedTask.category.id, equals(taskToUpdate.category.id));
      expect(updatedTask.priority.id, equals(taskToUpdate.priority.id));
      expect(updatedTask.archived, equals(taskToUpdate.archived));
      expect(updatedTask.done, equals(taskToUpdate.done));
      expect(updatedTask.user.userId, equals(taskToUpdate.user.userId));

      await taskFutures.deleteTask(updatedTask.taskId);
      final TaskModel? tasksAfterDeleting =
          await taskFutures.fetchTaskById(updatedTask.taskId);
      expect(tasksAfterDeleting, isNull,
          reason: 'Task should be deleted successfully');
    });

    test('updateStatus should update the status of an existing task', () async {
      final CreateCategory createCategory = CreateCategory(id: 1);
      final CreatePriority createPriority = CreatePriority(id: 1);
      final createUser = CreateUser(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
      );
      final CreateTask taskToUpdateStatus = CreateTask(
        taskId: 1,
        name: 'Test Task Update 2',
        description: 'Test Description',
        dueDate: now,
        done: false,
        priority: createPriority,
        category: createCategory,
        completedDate: null,
        archived: false,
        user: createUser,
      );

      await taskFutures.addTask(taskToUpdateStatus);

      final bool statusUpdated =
          await taskFutures.updateStatus(taskToUpdateStatus);

      expect(statusUpdated, isFalse,
          reason: 'Status should be updated successfully');

      final List<TaskModel> tasksAfterUpdate = await taskFutures.fetchTasks();

      final priority = Priority(id: 1, name: 'High');
      final category = Category(id: 1, name: 'Work');
      final user = UserDTO(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
        id: 1,
        email: 'test@test.de',
        name: 'Test',
        createdAt: DateTime.now(),
        updatedAt: DateTime.now(),
        role: 'user',

      );

      final updatedTask = tasksAfterUpdate.firstWhere(
        (task) => task.name == taskToUpdateStatus.name,
        orElse: () => TaskModel(
          taskId: -1,
          name: 'WRONG',
          description: 'WRONG',
          dueDate: now,
          done: false,
          priority: priority,
          category: category,
          completedDate: null,
          archived: false,
          user: user,
        ),
      );

      await taskFutures.deleteTask(updatedTask.taskId);
      final TaskModel? tasksAfterDeleting =
          await taskFutures.fetchTaskById(updatedTask.taskId);
      expect(tasksAfterDeleting, isNull,
          reason: 'Task should be deleted successfully');
    });

    test('updateTask should handle error for non-existing task', () async {
      final CreateCategory createCategory = CreateCategory(id: 1);
      final CreatePriority createPriority = CreatePriority(id: 1);
      final createUser = CreateUser(
        userId: 'qUOIUvY20wN8a3T8yG10NYR4Gl22',
      );

      final CreateTask nonExistingTask = CreateTask(
          taskId: -1,
          name: 'WRONG',
          description: 'WRONG',
          dueDate: now,
          done: false,
          priority: createPriority,
          category: createCategory,
          completedDate: null,
          archived: false,
          user: createUser);

      try {
        await taskFutures.updateTask(nonExistingTask);
        fail(
            'Expected an exception to be thrown for updating a non-existing task');
      } catch (e) {
        expect(e, isInstanceOf<Exception>());
      }
    });
  });
}
