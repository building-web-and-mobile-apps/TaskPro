import 'package:bwma_todo_app/core/TaskFutures.dart';
import 'package:bwma_todo_app/screens/AddTaskScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:bwma_todo_app/screens/TasksScreen.dart';

class MockTaskFutures extends Mock implements TaskFutures {}

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  group('TasksScreen Widget Tests', () {
    late MockNavigatorObserver navigatorObserver;
    late MockTaskFutures taskFutures;

    setUp(() {
      navigatorObserver = MockNavigatorObserver();
      taskFutures = MockTaskFutures();
    });

    testWidgets('Renders AddTaskScreen', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: AddTaskScreen(taskFutures: taskFutures,),
          navigatorObservers: [navigatorObserver],
        ),
      );

      expect(find.byType(AddTaskScreen), findsOneWidget);
    });
  });
}
