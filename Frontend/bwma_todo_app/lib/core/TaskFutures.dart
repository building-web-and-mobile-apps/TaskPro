import 'dart:convert';
import 'dart:typed_data';

import 'package:bwma_todo_app/model/Category.dart';
import 'package:bwma_todo_app/model/Priority.dart';
import 'package:bwma_todo_app/model/UserDTO.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import '../../model/Task.dart';
import '../model/Attachment.dart';

class TaskFutures {
  static const String baseUrl = 'http://localhost:8080';
  // static const String baseUrl = 'https://taskpro.francecentral.cloudapp.azure.com/api';
  // static const String baseUrl = 'http://192.168.178.55:8080';
  // static const String baseUrl = 'http://10.6.24.171:8080';

  final String authToken;

  TaskFutures(this.authToken);

  Map<String, String> get headers => {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer $authToken',
  };

  Map<String, String> get headersMultipart => {
    'Content-Type': 'multipart/form-data',
    'Authorization': 'Bearer $authToken',
  };

  Future<List<TaskModel>> fetchTasks() async {
    final url = Uri.parse('$baseUrl/tasks');
    try {
      final response = await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        final List<dynamic> jsonData = json.decode(response.body);

        List<TaskModel> tasks = jsonData.map((taskJson) {
          return TaskModel.fromJson(taskJson);
        }).toList();

        return tasks;
      } else {
        print('Failed with status code: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print('Error: $e');
      return [];
    }
  }

  Future<TaskModel?> fetchTaskById(int id) async {
    final url = Uri.parse('$baseUrl/tasks/$id');

    try {
      final response = await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        final Map<String, dynamic> jsonData = json.decode(response.body);
        TaskModel task = TaskModel.fromJson(jsonData);

        return task;
      } else {
        print('Failed with status code: ${response.statusCode}');
        return null;
      }
    } catch (e) {
      print('Error: $e');
      return null;
    }
  }

  Future<void> addTask(CreateTask task) async {
    final url = Uri.parse('$baseUrl/tasks');

    try {
      final response = await http.post(
        url,
        headers: headers,
        body: jsonEncode(task.toJson()),
      );

      if (response.statusCode == 201) {
        print('New task added successfully');
      } else {
        print(
            'Failed to add new task with status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error adding new task: $e');
    }
  }

  Future<void> deleteTask(int taskId) async {
    final url = Uri.parse('$baseUrl/tasks/$taskId');

    try {
      final response = await http.delete(
        url,
        headers: headers,
      );

      if (response.statusCode == 200) {
        print('Task $taskId deleted successfully');
      } else {
        print('Failed to delete task with status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error deleting task: $e');
    }
  }

  Future<bool> updateStatus(CreateTask task) async {
    final url = Uri.parse('$baseUrl/tasks/status/${task.taskId}');

    final requestBody = jsonEncode(task.toJson());

    try {
      final response = await http.put(
        url,
        headers: headers,
        body: requestBody,
      );

      if (response.statusCode == 200) {
        print('Status updated successfully');
        return true;
      } else {
        print(
            'Failed to update status with status code: ${response.statusCode}');
        return false;
      }
    } catch (e) {
      print('Error updating status: $e');
      return false;
    }
  }

  Future<void> updateTask(CreateTask task) async {
    final url = Uri.parse('$baseUrl/tasks/${task.taskId}');

    final requestBody = jsonEncode(task.toJson());

    try {
      final response = await http.put(
        url,
        headers: headers,
        body: requestBody,
      );

      if (response.statusCode == 200) {
        print('Task updated successfully');
      } else {
        print('Failed to update task with status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error updating task: $e');
    }
  }

  Future<List<TaskModel>> searchTasks(String keyword) async {
    final uid = FirebaseAuth.instance.currentUser?.uid;
    final url = Uri.parse('$baseUrl/tasks/search?keyword=$keyword&userId=$uid');
    try {
      final response = await http.get(url, headers: headers);
      if (response.body == 'No tasks found matching your search!') {
        return [];
      } else {
        final List<dynamic> jsonData = json.decode(response.body);

        List<TaskModel> tasks = jsonData.map((taskJson) {
          return TaskModel.fromJson(taskJson);
        }).toList();

        return tasks;
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<TaskModel>> sortTasks(String sortBy, String direction) async {
    final uid = FirebaseAuth.instance.currentUser?.uid;
    final url =
        Uri.parse('$baseUrl/tasks/sort?sortBy=$sortBy&direction=$direction&userId=$uid');
    try {
      final response = await http.get(url, headers: headers);
      if (response.statusCode == 200) {
        final List<dynamic> jsonData = json.decode(response.body);

        List<TaskModel> tasks = jsonData.map((taskJson) {
          return TaskModel.fromJson(taskJson);
        }).toList();

        return tasks;
      } else {
        print('Failed with status code: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<TaskModel>> filterTasks(
      String priorityId, String dueDate, String done) async {
    final uid = FirebaseAuth.instance.currentUser?.uid;
    final url = Uri.parse(
        '$baseUrl/tasks/filter?priorityId=$priorityId&dueDate=$dueDate&done=$done&userId=$uid');
    try {
      final response = await http.get(url, headers: headers);
      if (response.statusCode == 200) {
        if (response.body == 'No tasks found matching your search!') {
          return [];
        } else {
          final List<dynamic> jsonData = json.decode(response.body);

          List<TaskModel> tasks = jsonData.map((taskJson) {
            return TaskModel.fromJson(taskJson);
          }).toList();

          return tasks;
        }
      } else {
        print('Failed with status code: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<void> createUser(String email, String name, String userId) async {
    final url = Uri.parse('$baseUrl/users');
    final Map<String, dynamic> userData = {
      'email': email,
      'name': name,
      'userId': userId,
      'createdAt': DateTime.now().toIso8601String(),
      'updatedAt': DateTime.now().toIso8601String(),
    };

    try {
      final response = await http.post(
        url,
        headers: headers,
        body: jsonEncode(userData),
      );

      if (response.statusCode == 201) {
        print('New user created successfully');
        FirebaseFirestore.instance.collection('users').doc(userId).update({
          'admin': false,
        });
      } else {
        print('Failed to create user with status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error creating user: $e');
    }
  }

  Future<List<TaskModel>> fetchFilteredTasks() async {
    final url = Uri.parse('$baseUrl/tasks');
    try {
      final response = await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        final List<dynamic> jsonData = json.decode(response.body);

        final loggedInUserId = FirebaseAuth.instance.currentUser?.uid;

        // Filter tasks by userId
        List<TaskModel> filteredTasks = jsonData
            .map((taskJson) => TaskModel.fromJson(taskJson))
            .where((task) => task.user.userId == loggedInUserId)
            .toList();

        return filteredTasks;
      } else {
        print('Failed with status code: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print('Error: $e');
      return [];
    }
  }

  Future<List<Category>> fetchCategories() async {
    final url = Uri.parse('$baseUrl/categories');
    try {
      final response = await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        final List<dynamic> jsonData = json.decode(response.body);

        List<Category> categories = jsonData.map((categoryJson) {
          return Category.fromJson(categoryJson);
        }).toList();

        return categories;
      } else {
        print('Failed with status code: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print('Error: $e');
      return [];
    }
  }

  Future<void> addCategory(Category category) async {
    final url = Uri.parse('$baseUrl/categories');

    try {
      final response = await http.post(
        url,
        headers: headers,
        body: jsonEncode(category.toJson()),
      );

      if (response.statusCode == 201) {
        print('New category added successfully');
      } else {
        print(
            'Failed to add new category with status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error adding new category: $e');
    }
  }

  Future<List<Priority>> fetchPriorities() async {
    final url = Uri.parse('$baseUrl/priorities');
    try {
      final response = await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        final List<dynamic> jsonData = json.decode(response.body);

        List<Priority> priorities = jsonData.map((priorityJson) {
          return Priority.fromJson(priorityJson);
        }).toList();

        return priorities;
      } else {
        print('Failed with status code: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print('Error: $e');
      return [];
    }
  }

  Future<bool> getUserByUserId(String uid) async {
    final url = Uri.parse('$baseUrl/users?userId=$uid');
    try {
      final response = await http.get(url, headers: headers);

      if (response.statusCode == 200) {
        final Map<String, dynamic> jsonData = json.decode(response.body);
        UserDTO.fromJson(jsonData);

        return true;
      } else {
        print('Failed with status code: ${response.statusCode}');
        return false;
      }
    } catch (e) {
      print('Error: $e');
      return false;
    }
  }

  Future<String?> getUserRoleByUserId(String uid) async {
  final url = Uri.parse('$baseUrl/users?userId=$uid');
  try {
    final response = await http.get(url, headers: headers);

    if (response.statusCode == 200) {
      final Map<String, dynamic> jsonData = json.decode(response.body);
      UserDTO user = UserDTO.fromJson(jsonData);

      return user.role;
    } else {
      print('Failed with status code: ${response.statusCode}');
      return null;
    }
  } catch (e) {
    print('Error: $e');
    return null;
  }
}

  Future<bool> addBulkTasks(String content) async {
    try {
      List<String> lines = content.split('\n');
      lines = lines.map((line) => line.replaceAll('\r', '')).toList();

      List<Map<String, dynamic>> taskItems = [];

      for (int i = 1; i < lines.length; i++) {
        List<String> fields = lines[i].split(';');

        Map<String, dynamic> task = {
          "title": fields[0],
          "description": fields[1],
          "done": fields[2] == 'true',
          "dueDate": fields[3],
          "completeDate": fields[4],
          "archived": fields[5] == 'true',
          "user": {
            "userId": FirebaseAuth.instance.currentUser?.uid,
          },
          "priority": {
            "id": _parsePriority(fields[6]),
          },
          "category": {
            "id": _parseCategory(fields[7]),
          }
        };

        taskItems.add(task);
      }

      Map<String, dynamic> requestBody = {"taskItems": taskItems};

      final response = await http.post(
        Uri.parse('$baseUrl/tasks/bulk'),
        body: jsonEncode(requestBody),
        headers: headers,
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        print('Bulk tasks added successfully');
        return true;
      } else {
        print('Failed to add bulk tasks: ${response.statusCode}');
        return false;
      }
    } catch (e) {
      print('Error adding bulk tasks: $e');
      return false;
    }
  }

  int _parsePriority(String priority) {
    switch (priority.toLowerCase()) {
      case 'high':
        return 3;
      case 'medium':
        return 2;
      case 'low':
        return 1;
      default:
        return 0;
    }
  }

  int _parseCategory(String category) {
    switch (category.toLowerCase()) {
      case 'work':
        return 1;
      case 'personal':
        return 2;
      case 'other':
        return 3;
      default:
        return 0;
    }
  }

  Future<bool> uploadFileToTask(Uint8List fileBytes, int taskId, String fileName) async {
    final url = Uri.parse('$baseUrl/tasks/$taskId/attachments');

    if (fileName.contains(' ')) {
      fileName = fileName.replaceAll(' ', '_');
    }

    try {
      var request = http.MultipartRequest('POST', url)
        ..headers.addAll(headersMultipart)
        ..files.add(http.MultipartFile.fromBytes('files', fileBytes, filename: fileName));
      var response = await request.send();

      if (response.statusCode == 200) {
        print('File uploaded successfully');
        return true;
      } else {
        print('Failed to upload file with status code: ${response.statusCode}');
        return false;
      }
    } catch (e) {
      print('Error uploading file: $e');
      return false;
    }
  }

  Future<List<Attachment>> fetchAttachmentsByTaskId(int taskId) async {
    final url = Uri.parse('$baseUrl/tasks/$taskId/attachments');
    try {
      final response = await http.get(
        url,
        headers: headers
      );

      if (response.statusCode == 200) {
        final List<dynamic> jsonData = json.decode(response.body);

        List<Attachment> attachments = jsonData.map((attachmentJson) {
          return Attachment.fromJson(attachmentJson);
        }).toList();

        return attachments;
      } else {
        print('Failed to fetch attachments with status code: ${response.statusCode}');
        return [];
      }
    } catch (e) {
      print('Error fetching attachments: $e');
      return [];
    }
  }

  Future<bool> deleteAttachmentByTask(int taskId, int attachmentId) async {
    final url = Uri.parse('$baseUrl/tasks/$taskId/attachments/$attachmentId');

    try {
      final response = await http.delete(
        url,
        headers: headers,
      );

      if (response.statusCode == 200) {
        print('Attachment $attachmentId deleted successfully');
        return true;
      } else {
        print('Failed to delete attachment with status code: ${response.statusCode}');
        return false;
      }
    } catch (e) {
      print('Error deleting attachment: $e');
      return false;
    }
  }

  void downloadAttachment(String filePath) async {
    Reference ref = FirebaseStorage.instance.ref().child(filePath);
    var url = await ref.getDownloadURL();

    _launchURL(url);
  }

  Future<void> _launchURL(String url) async {
    Uri uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> addAdmin(String uid) async {
  final url = Uri.parse('$baseUrl/admin/add-admin');

  final Map<String, dynamic> adminData = {
    'uid': uid,
  };

  try {
    final response = await http.post(
      url,
      headers: headers,
      body: jsonEncode(adminData),
    );

    if (response.statusCode == 200) {
      print('Admin added successfully');
      // Update the 'admin' property in Firebase
      FirebaseFirestore.instance.collection('users').doc(uid).update({
        'admin': true,
      });
    } else {
      print('Failed to add admin with status code: ${response.statusCode}');
    }
  } catch (e) {
    print('Error adding admin: $e');
  }
}

Future<void> removeAdmin(String uid) async {
  final url = Uri.parse('$baseUrl/admin/remove-admin');

  final Map<String, dynamic> adminData = {
    'uid': uid,
  };

  try {
    final response = await http.post(
      url,
      headers: headers,
      body: jsonEncode(adminData),
    );

    if (response.statusCode == 200) {
      print('Admin removed successfully');
      // Update the 'admin' property in Firebase
      FirebaseFirestore.instance.collection('users').doc(uid).update({
        'admin': false,
      });
    } else {
      print('Failed to remove admin with status code: ${response.statusCode}');
    }
  } catch (e) {
    print('Error removing admin: $e');
  }
}

Future<void> removeAccount(String uid) async {
  final url = Uri.parse('$baseUrl/admin/remove-account');

  final Map<String, dynamic> adminData = {
    'uid': uid,
  };

  try {
    final response = await http.post(
      url,
      headers: headers,
      body: jsonEncode(adminData),
    );

    if (response.statusCode == 200) {
      print('Account removed successfully');
      FirebaseFirestore firestore = FirebaseFirestore.instance;
      await firestore.collection('users').doc(uid).delete();
      // Update the 'admin' property in Firebase
    } else {
      print('Failed to remove account with status code: ${response.statusCode}');
    }
  } catch (e) {
    print('Error removing account: $e');
  }
}
}
