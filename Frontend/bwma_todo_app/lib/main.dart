import 'package:bwma_todo_app/core/provider/FirebaseProvider.dart';
import 'package:bwma_todo_app/screens/ErrorScreen.dart';
import 'package:bwma_todo_app/screens/TasksScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_ui_oauth_google/firebase_ui_oauth_google.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'core/TaskFutures.dart';
import 'firebase_options.dart';

import 'package:firebase_auth/firebase_auth.dart' hide EmailAuthProvider;
import 'package:firebase_ui_auth/firebase_ui_auth.dart';

late String token;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
//how do i get the token of my current user firebase?
  //https://firebase.google.com/docs/auth/admin/verify-id-tokens
  // Ideal time to initialize
  //await FirebaseAuth.instance.useAuthEmulator('localhost', 9099);

  //https://firebase.google.com/docs/auth/flutter/start
  FirebaseAuth.instance.authStateChanges().listen((User? user) async {
    if (user == null) {
      print('User is currently signed out!');
    } else {
      print('id: ' + FirebaseAuth.instance.currentUser!.uid);
      print('id: ' +
          FirebaseAuth.instance.currentUser!.metadata.creationTime.toString());
      print('id: ' +
          FirebaseAuth.instance.currentUser!.metadata.lastSignInTime
              .toString());
      print('id: ' + FirebaseAuth.instance.currentUser!.displayName.toString());

      token = (await FirebaseAuth.instance.currentUser!.getIdToken())!;
      // print('token: $token');

      String uid = FirebaseAuth.instance.currentUser!.uid;
      String? displayName =
          FirebaseAuth.instance.currentUser!.email?.split("@")[0];
      String? email = FirebaseAuth.instance.currentUser!.email;
      String? photoURL = '';

      FirebaseFirestore firestore = FirebaseFirestore.instance;

      TaskFutures taskFutures = TaskFutures(token);
      bool userExists = await taskFutures.getUserByUserId(uid);
      if (!userExists) {
        Map<String, dynamic> userData = {
          'email': email,
          'name': displayName,
          'image': photoURL,
          'isOnline': true,
          'lastActive': Timestamp.now(),
          'uid': uid,
        };
        await firestore.collection('users').doc(uid).set(userData);

        taskFutures.createUser(email!, displayName!, uid);
      }

      runApp(MyApp());
    }
  });
  runApp(MyApp());
}

//https://firebase.google.com/codelabs/firebase-auth-in-flutter-apps#0
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => FirebaseProvider(),
      child: MaterialApp(
        initialRoute: FirebaseAuth.instance.currentUser == null
            ? '/sign-in'
            : '/taskScreen',
        routes: {
          '/sign-in': (context) {
            return SignInScreen(
              providers: [
                EmailAuthProvider(),
                GoogleProvider(
                    clientId:
                        "255803640690-2vur55urmdguv7v2e5gdvu60oihk1m7q.apps.googleusercontent.com"),
              ],
              actions: [
                AuthStateChangeAction<SignedIn>((context, state) {
                  Navigator.pushReplacementNamed(context, '/taskScreen');
                }),
              ],
            );
          },
          '/profile': (context) {
            return ProfileScreen(
              providers: [
                EmailAuthProvider(), // new
              ],
              actions: [
                SignedOutAction((context) {
                  Navigator.pushReplacementNamed(context, '/sign-in');
                }),
              ],
            );
          },
          '/taskScreen': (context) {
            return TasksScreen(taskFutures: TaskFutures(token), token: token);
          },
          '/error': (context) {
            return ErrorScreen(errorMessage: 'This is a sample error message.');
          },
        },
      ),
    );
  }
}
