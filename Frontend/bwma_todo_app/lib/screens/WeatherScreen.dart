import 'dart:convert';

import 'package:bwma_todo_app/core/TaskFutures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class WeatherScreen extends StatefulWidget {
  final String token;

  const WeatherScreen({Key? key, required this.token}) : super(key: key);

  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class WeatherInfo {
  final String cityName;
  final double temperatureCelsius;
  final double pressure;
  final int humidity;

  WeatherInfo({
    required this.cityName,
    required this.temperatureCelsius,
    required this.pressure,
    required this.humidity,
  });

  factory WeatherInfo.fromJson(Map<String, dynamic> json) {
    return WeatherInfo(
      cityName: json['name'] ?? '',
      temperatureCelsius: (json['main']['temp'] ?? 0).toDouble() - 273.15,
      pressure: (json['main']['pressure'] ?? 0).toDouble(),
      humidity: (json['main']['humidity'] ?? 0).toInt(),
    );
  }
}

class _WeatherScreenState extends State<WeatherScreen> {
  final TextEditingController _cityController = TextEditingController();
  WeatherInfo? _weatherInfo;
  bool _hasEnteredCity = false;

  Future<void> _getWeatherData(String city) async {
    const baseUrlBe = TaskFutures.baseUrl;
    final headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ${widget.token}',
    };
    final response =
    await http.get(Uri.parse('$baseUrlBe/weather/$city'), headers: headers);

    if (response.statusCode == 200) {
      setState(() {
        _weatherInfo = WeatherInfo.fromJson(json.decode(response.body));
        _hasEnteredCity = true; // Set to true when weather data is successfully loaded
      });
    } else {
      setState(() {
        _weatherInfo = null;
        _hasEnteredCity = true; // Set to true when there's an error to show the message
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
        const Text('Weather', style: TextStyle(color: Color(0xFFE4E1E6))),
        backgroundColor: Color.alphaBlend(
          fromInt(0xFFB7C4FF).withOpacity(0.14),
          fromInt(0xFF1B1B1F),
        ),
        iconTheme: const IconThemeData(color: Color(0xFFE4E1E6)),
      ),
      body: Container(
        decoration: BoxDecoration(
          color: Color.alphaBlend(
            fromInt(0xFFB7C4FF).withOpacity(0.14),
            fromInt(0xFF1B1B1F),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              TextField(
                style: const TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                controller: _cityController,
                decoration: const InputDecoration(
                  labelText: 'Enter City',
                  border: OutlineInputBorder(),
                  labelStyle: TextStyle(
                    color: Color(0xFFE4E1E6),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFE4E1E6),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFE4E1E6),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 16),
              ElevatedButton(
                onPressed: () async {
                  await _getWeatherData(_cityController.text);
                },
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: const Color(0xFF094EE7),
                ),
                child: const Text('Get Weather'),
              ),
              const SizedBox(height: 16),
              if (_hasEnteredCity && _weatherInfo != null)
                _buildWeatherInfoCard(_weatherInfo!)
              else if (_hasEnteredCity)
                const Text(
                  'Failed to load weather data',
                  style: TextStyle(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildWeatherInfoCard(WeatherInfo weatherInfo) {
    return Card(
      elevation: 4,
      color: Color.alphaBlend(
        fromInt(0xFFB7C4FF).withOpacity(0.14),
        fromInt(0xFF1B1B1F),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'City: ${weatherInfo.cityName}',
              style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            const SizedBox(height: 8),
            Text(
              'Temperature: ${weatherInfo.temperatureCelsius.toStringAsFixed(2)}°C',
              style: const TextStyle(fontSize: 16, color: Colors.white),
            ),
            const SizedBox(height: 8),
            Text(
              'Pressure: ${weatherInfo.pressure.toString()} hPa',
              style: const TextStyle(fontSize: 16, color: Colors.white),
            ),
            const SizedBox(height: 8),
            Text(
              'Humidity: ${weatherInfo.humidity.toString()}%',
              style: const TextStyle(fontSize: 16, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }

  static Color fromInt(int hexInt) {
    return Color(hexInt);
  }
}
