import 'package:bwma_todo_app/core/TaskFutures.dart';
import 'package:bwma_todo_app/model/Category.dart';
import 'package:bwma_todo_app/model/Priority.dart';
import 'package:bwma_todo_app/model/UserDTO.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../model/Task.dart';

class EditTasksScreen extends StatefulWidget {
  final TaskModel? task;
  final int taskId;
  final TaskFutures taskFutures;

  const EditTasksScreen(
      {super.key, required this.task, required this.taskId, required this.taskFutures});

  @override
  _EditTasksScreenState createState() => _EditTasksScreenState();
}

class _EditTasksScreenState extends State<EditTasksScreen> {
  late TextEditingController _nameController;
  late TextEditingController _descriptionController;
  late TextEditingController _dueDateController;
  late TextEditingController _completedDateController;
  late bool _isDone;
  late bool _isArchived;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController(text: widget.task?.name);
    _descriptionController =
        TextEditingController(text: widget.task?.description);
    _dueDateController =
        TextEditingController(text: widget.task?.dueDate.toString());
    // _priorityController = TextEditingController(text: widget.task?.priority.name);
    // _categoryController = TextEditingController(text: widget.task?.category.name);
    _completedDateController = TextEditingController(
        text: widget.task?.completedDate?.toString() ?? "");
    _isDone = widget.task!.done;
    _isArchived = widget.task!.archived;
  }

  @override
  void dispose() {
    _nameController.dispose();
    _descriptionController.dispose();
    _dueDateController.dispose();
    _completedDateController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.alphaBlend(
          fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
      appBar: AppBar(
        backgroundColor: Color.alphaBlend(
            fromInt(0xFFB7C4FF).withOpacity(0.08), fromInt(0xFF1B1B1F)),
        title: const Text(
          'Edit Task',
          style: TextStyle(color: Color(0xFFE4E1E6)),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Color(0xFFE4E1E6),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SingleChildScrollView(
        child: TaskForm(
          taskFutures: widget.taskFutures,
          nameController: _nameController,
          descriptionController: _descriptionController,
          dueDateController: _dueDateController,
          completedDateController: _completedDateController,
          isDone: _isDone,
          isArchived: _isArchived,
          taskId: widget.taskId,
          onChangedDone: (value) {
            setState(() {
              _isDone = value;
            });
          },
          onChangedArchived: (value) {
            setState(() {
              _isArchived = value;
            });
          },
          priority: widget.task!.priority,
          category: widget.task!.category,
        ),
      ),
    );
  }

  static Color fromInt(int hexInt) {
    return Color(hexInt);
  }
}

class TaskForm extends StatefulWidget {
  final int taskId;
  final TextEditingController nameController;
  final TextEditingController descriptionController;
  final TextEditingController dueDateController;
  final TextEditingController completedDateController;
  final bool isDone;
  final bool isArchived;
  final ValueChanged<bool> onChangedDone;
  final ValueChanged<bool> onChangedArchived;
  final TaskFutures taskFutures;
  final Priority priority;
  final Category category;

  const TaskForm({
    super.key,
    required this.nameController,
    required this.descriptionController,
    required this.dueDateController,
    required this.completedDateController,
    required this.isDone,
    required this.isArchived,
    required this.onChangedDone,
    required this.onChangedArchived,
    required this.taskId,
    required this.taskFutures,
    required this.priority,
    required this.category,
  });

  @override
  _TaskFormState createState() => _TaskFormState();
}

class _TaskFormState extends State<TaskForm> {
  final _formKey = GlobalKey<FormState>();

  List<Category> _categories = [];
  Category? _selectedCategory;

  List<Priority> _priorities = [];
  Priority? _selectedPriority;

  @override
  void initState() {
    super.initState();
    _fetchCategories();
    _fetchPriorities();
  }

  Future<void> _fetchCategories() async {
    try {
      List<Category> categories = await widget.taskFutures.fetchCategories();
      setState(() {
        _categories = categories;
        _selectedCategory = _categories.firstWhere(
                (category) => category.id == widget.category.id,
            orElse: () => _categories.first);
      });
    } catch (e) {
      print('Error fetching categories: $e');
    }
  }

  Future<void> _fetchPriorities() async {
    try {
      List<Priority> priorities = await widget.taskFutures.fetchPriorities();
      setState(() {
        _priorities = priorities;
        _selectedPriority = _priorities.firstWhere(
                (priority) => priority.id == widget.priority.id,
            orElse: () => _priorities.first);
      });
    } catch (e) {
      print('Error fetching priorities: $e');
    }
  }

  Future<void> _selectDueDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != widget.dueDateController.text) {
      setState(() {
        widget.dueDateController.text = picked.toString();
      });
    }
  }

  Future<void> _selectCompletedDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != widget.completedDateController.text) {
      setState(() {
        widget.completedDateController.text = picked.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              controller: widget.nameController,
              decoration: const InputDecoration(
                labelText: 'Name',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter the task name';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            TextFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              controller: widget.descriptionController,
              decoration: const InputDecoration(
                labelText: 'Description',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter the task description';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            CheckboxListTile(
              title: const Text(
                'Done',
                style: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
              ),
              value: widget.isDone,
              onChanged: (value) {
                widget.onChangedDone(value!);
              },
              controlAffinity: ListTileControlAffinity.leading,
              activeColor: const Color(0xFF094EE7),
            ),
            const SizedBox(height: 20),
            TextFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              controller: widget.dueDateController,
              onTap: () => _selectDueDate(context),
              decoration: const InputDecoration(
                labelText: 'Due Date',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter the task due date';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            DropdownButtonFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              decoration: const InputDecoration(
                labelText: 'Priority',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              value: _selectedPriority,
              items: _priorities
                  .map((priority) =>
                  DropdownMenuItem(
                    value: priority,
                    child: Text(priority.name),
                  ))
                  .toList(),
              onChanged: (value) {
                setState(() {
                  _selectedPriority = value as Priority;
                });
              },
              validator: (value) {
                if (value == null) {
                  return 'Please select a priority';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            DropdownButtonFormField<Category>(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              decoration: const InputDecoration(
                labelText: 'Category',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              value: _selectedCategory,
              items: _categories
                  .map((category) =>
                  DropdownMenuItem<Category>(
                    value: category,
                    child: Text(
                      category.name,
                    ),
                  ))
                  .toList(),
              onChanged: (Category? value) {
                setState(() {
                  _selectedCategory = value;
                });
              },
              validator: (value) {
                if (value == null) {
                  return 'Please select a category';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            TextFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              controller: widget.completedDateController,
              onTap: () => _selectCompletedDate(context),
              decoration: const InputDecoration(
                labelText: 'Completed Date',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            CheckboxListTile(
              title: const Text(
                'Archived',
                style: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
              ),
              value: widget.isArchived,
              onChanged: (value) {
                widget.onChangedArchived(value!);
              },
              controlAffinity: ListTileControlAffinity.leading,
              activeColor: const Color(0xFF094EE7),
            ),
            const SizedBox(height: 20),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        String? uid = FirebaseAuth.instance.currentUser?.uid;
                        if (uid == null) {
                          return;
                        }
                        CreateUser createUser = CreateUser(
                          userId: uid,
                        );

                        CreatePriority createPriority = CreatePriority(
                          id: _selectedPriority!.id,
                        );

                        CreateCategory createCategory = CreateCategory(
                          id: _selectedCategory!.id,
                        );

                        final updatedTask = CreateTask(
                          taskId: widget.taskId,
                          name: widget.nameController.text,
                          description: widget.descriptionController.text,
                          dueDate: DateTime.parse(
                              widget.dueDateController.text),
                          priority: createPriority,
                          category: createCategory,
                          completedDate: widget.completedDateController.text
                              .isEmpty
                              ? null
                              : DateTime.parse(
                              widget.completedDateController.text),
                          done: widget.isDone,
                          archived: widget.isArchived,
                          user: createUser,
                        );

                        // final updatedTask = Task(
                        //   taskId: widget.taskId,
                        //   name: widget.nameController.text,
                        //   description: widget.descriptionController.text,
                        //   dueDate: DateTime.parse(widget.dueDateController.text),
                        //   priority: widget.priorityController.text,
                        //   category: widget.categoryController.text,
                        //   completedDate: widget.completedDateController.text.isEmpty
                        //       ? null
                        //       : DateTime.parse(widget.completedDateController.text),
                        //   done: widget.isDone,
                        //   archived: widget.isArchived,
                        // );

                        await widget.taskFutures.updateTask(updatedTask);

                        Navigator.of(context).pop(true);
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: const Color(0xFF094EE7),
                    ),
                    child: const Text('Save'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    style: ElevatedButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: const Color(0xFF094EE7),
                    ),
                    child: const Text('Cancel'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
