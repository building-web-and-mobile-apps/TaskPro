import 'package:bwma_todo_app/core/TaskFutures.dart';
import 'package:bwma_todo_app/model/Task.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../model/Category.dart';
import '../model/Priority.dart';
import '../model/UserDTO.dart';

class AddTaskScreen extends StatelessWidget {
  final TaskFutures taskFutures;

  AddTaskScreen({Key? key, required this.taskFutures}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.alphaBlend(
          fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
      appBar: AppBar(
        backgroundColor: Color.alphaBlend(
            fromInt(0xFFB7C4FF).withOpacity(0.08), fromInt(0xFF1B1B1F)),
        title: const Text(
          'Add Task',
          style: TextStyle(color: Color(0xFFE4E1E6)),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Color(0xFFE4E1E6),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SingleChildScrollView(
        child: TaskForm(
          taskFutures: taskFutures,
        ),
      ),
    );
  }

  static Color fromInt(int hexInt) {
    return Color(hexInt);
  }
}

class TaskForm extends StatefulWidget {
  final TaskFutures taskFutures;

  TaskForm({Key? key, required this.taskFutures}) : super(key: key);

  @override
  _TaskFormState createState() => _TaskFormState();
}

class _TaskFormState extends State<TaskForm> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _dueDateController = TextEditingController();
  final TextEditingController _priorityController = TextEditingController();
  final TextEditingController _categoryController = TextEditingController();
  final TextEditingController _completedDateController =
      TextEditingController();
  late bool _isDone = false;
  late bool _isArchived = false;

  List<Category> _categories = [];
  Category? _selectedCategory;

  List<Priority> _priorities = [];
  Priority? _selectedPriority;

  @override
  void initState() {
    super.initState();
    _fetchCategories();
    _fetchPriorities();
  }

  Future<void> _fetchCategories() async {
    try {
      List<Category> categories = await widget.taskFutures.fetchCategories();
      setState(() {
        _categories = categories;
        _selectedCategory = categories.first;
      });
    } catch (e) {
      print('Error fetching categories: $e');
    }
  }

  Future<void> _fetchPriorities() async {
    try {
      List<Priority> priorities = await widget.taskFutures.fetchPriorities();
      setState(() {
        _priorities = priorities;
        _selectedPriority = priorities.first;
      });
    } catch (e) {
      print('Error fetching priorities: $e');
    }
  }

  Future<void> _selectDueDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _dueDateController.text) {
      setState(() {
        _dueDateController.text = picked.toString();
      });
    }
  }

  Future<void> _selectCompletedDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2100),
    );

    if (picked != null && picked != _completedDateController.text) {
      setState(() {
        _completedDateController.text = picked.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              controller: _nameController,
              decoration: const InputDecoration(
                labelText: 'Name',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter the task name';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            TextFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              controller: _descriptionController,
              decoration: const InputDecoration(
                labelText: 'Description',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter the task description';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            CheckboxListTile(
              title: const Text(
                'Done',
                style: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
              ),
              value: _isDone,
              onChanged: (bool? value) {
                setState(() {
                  _isDone = value!;
                });
              },
              controlAffinity: ListTileControlAffinity.leading,
              activeColor: const Color(0xFF094EE7),
            ),
            const SizedBox(height: 20),
            TextFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              controller: _dueDateController,
              decoration: const InputDecoration(
                labelText: 'Due Date',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              onTap: () {
                _selectDueDate(context);
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter the due date';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            DropdownButtonFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              decoration: const InputDecoration(
                labelText: 'Priority',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              value: _selectedPriority,
              items: _priorities
                  .map((priority) => DropdownMenuItem(
                        value: priority,
                        child: Text(priority.name),
                      ))
                  .toList(),
              onChanged: (value) {
                setState(() {
                  _selectedPriority = value as Priority;
                });
              },
              validator: (value) {
                if (value == null) {
                  return 'Please enter the priority';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            DropdownButtonFormField<Category>(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              decoration: const InputDecoration(
                labelText: 'Category',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              value: _selectedCategory,
              items: _categories
                  .map((category) => DropdownMenuItem<Category>(
                        value: category,
                        child: Text(
                          category.name,
                        ),
                      ))
                  .toList(),
              onChanged: (Category? value) {
                setState(() {
                  _selectedCategory = value;
                });
              },
              validator: (value) {
                if (value == null) {
                  return 'Please enter the category';
                }
                return null;
              },
            ),
            const SizedBox(height: 20),
            TextFormField(
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
              controller: _completedDateController,
              decoration: const InputDecoration(
                labelText: 'Completed Date',
                border: OutlineInputBorder(),
                labelStyle: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color(0xFFE4E1E6),
                  ),
                ),
              ),
              onTap: () {
                _selectCompletedDate(context);
              },
            ),
            const SizedBox(height: 20),
            CheckboxListTile(
              title: const Text(
                'Archived',
                style: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
              ),
              value: _isArchived,
              onChanged: (bool? value) {
                setState(() {
                  _isArchived = value!;
                });
              },
              controlAffinity: ListTileControlAffinity.leading,
              activeColor: const Color(0xFF094EE7),
            ),
            const SizedBox(height: 20),
            Center(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  key: const ValueKey('saveButton'),
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      String? uid = FirebaseAuth.instance.currentUser?.uid;
                      if (uid == null) {
                        return;
                      }
                      CreateUser createUser = CreateUser(
                        userId: uid,
                      );

                      CreatePriority createPriority = CreatePriority(
                        id: _selectedPriority!.id,
                      );

                      CreateCategory createCategory = CreateCategory(
                        id: _selectedCategory!.id,
                      );

                      CreateTask newTask = CreateTask(
                        taskId: 1,
                        name: _nameController.text,
                        description: _descriptionController.text,
                        done: _isDone,
                        dueDate: DateTime.parse(_dueDateController.text),
                        priority: createPriority,
                        category: createCategory,
                        completedDate: _completedDateController.text.isEmpty
                            ? null
                            : DateTime.parse(_completedDateController.text),
                        archived: _isArchived,
                        user: createUser,
                      );

                      await widget.taskFutures.addTask(newTask);

                      Navigator.of(context).pop(true);
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color(0xFF094EE7),
                  ),
                  child: const Text('Save'),
                ),
                ElevatedButton(
                  onPressed: () {
                    _formKey.currentState!.reset();
                  },
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color(0xFF094EE7),
                  ),
                  child: const Text('Reset'),
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                    foregroundColor: Colors.white,
                    backgroundColor: const Color(0xFF094EE7),
                  ),
                  child: const Text('Cancel'),
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _nameController.dispose();
    _descriptionController.dispose();
    _dueDateController.dispose();
    _priorityController.dispose();
    _categoryController.dispose();
    _completedDateController.dispose();
    super.dispose();
  }
}
