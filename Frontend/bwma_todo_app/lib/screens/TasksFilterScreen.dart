import 'package:flutter/material.dart';

import '../core/TaskFutures.dart';
import '../model/Priority.dart';
import '../model/Task.dart';

class TaskFilterScreen extends StatefulWidget {
  final TaskFutures taskFutures;

  TaskFilterScreen({
    Key? key,
    required this.taskFutures,
  }) : super(key: key);

  @override
  _TaskFilterScreenState createState() => _TaskFilterScreenState();
}

class _TaskFilterScreenState extends State<TaskFilterScreen> {
  TextEditingController dueDateController = TextEditingController();
  String doneValue = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final List<Priority> _priorities = [Priority(name: "All", id: 0)];
  Priority? _selectedPriority;

  @override
  void initState() {
    super.initState();
    _fetchPriorities();
  }

  Future<void> _fetchPriorities() async {
    try {
      List<Priority> priorities = await widget.taskFutures.fetchPriorities();
      setState(() {
        _priorities.addAll(priorities);
        _selectedPriority = _priorities[0];
      });
    } catch (e) {
      print('Error fetching priorities: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.alphaBlend(
          fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Color(0xFFE4E1E6),
        ),
        title: const Text(
          'Filter Tasks',
          style: TextStyle(
            color: Color(0xFFE4E1E6),
          ),
        ),
        backgroundColor: Color.alphaBlend(
            fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              DropdownButtonFormField(
                style: const TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                decoration: const InputDecoration(
                  labelText: 'Priority',
                  border: OutlineInputBorder(),
                  labelStyle: TextStyle(
                    color: Color(0xFFE4E1E6),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFE4E1E6),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFE4E1E6),
                    ),
                  ),
                ),
                value: _selectedPriority,
                items: _priorities
                    .map((priority) => DropdownMenuItem(
                          value: priority,
                          child: Text(priority.name),
                        ))
                    .toList(),
                onChanged: (value) {
                  setState(() {
                    _selectedPriority = value as Priority;
                  });
                },
                validator: (value) {
                  if (value == null) {
                    return 'Please select a priority';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 16),
              TextFormField(
                style: const TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                controller: dueDateController,
                decoration: const InputDecoration(
                  labelText: 'Due Date (yyyy-MM-dd)',
                  border: OutlineInputBorder(),
                  labelStyle: TextStyle(
                    color: Color(0xFFE4E1E6),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFE4E1E6),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFE4E1E6),
                    ),
                  ),
                ),
                validator: (value) {
                  RegExp dateRegExp = RegExp(r'^$|^(\d{4}-\d{2}-\d{2})$');

                  if (!dateRegExp.hasMatch(value!)) {
                    return 'Invalid date format. Please use yyyy-MM-dd';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 16),
              DropdownButtonFormField<String>(
                style: const TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
                dropdownColor: Color.alphaBlend(
                    fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
                value: doneValue,
                items: const [
                  DropdownMenuItem(
                    value: 'true',
                    child: Text(
                      'True',
                      style: TextStyle(
                        color: Color(0xFFE4E1E6),
                      ),
                    ),
                  ),
                  DropdownMenuItem(
                    value: 'false',
                    child: Text(
                      'False',
                      style: TextStyle(
                        color: Color(0xFFE4E1E6),
                      ),
                    ),
                  ),
                  DropdownMenuItem(
                    value: '',
                    child: Text(
                      'All',
                      style: TextStyle(
                        color: Color(0xFFE4E1E6),
                      ),
                    ),
                  )
                ],
                onChanged: (value) {
                  setState(() {
                    doneValue = value!;
                  });
                },
                decoration: const InputDecoration(
                  labelText: 'Done',
                  border: OutlineInputBorder(),
                  labelStyle: TextStyle(
                    color: Color(0xFFE4E1E6),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFE4E1E6),
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Color(0xFFE4E1E6),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 16),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: const Color(0xFF094EE7),
                ),
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    List<TaskModel> filteredTasks = await widget.taskFutures
                        .filterTasks(
                            _selectedPriority!.name == 'All'
                                ? ''
                                : _selectedPriority!.id.toString(),
                            dueDateController.text,
                            doneValue);
                    Navigator.of(context).pop(filteredTasks);
                  }
                },
                child: const Text('Apply Filters'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Color fromInt(int hexInt) {
    return Color(hexInt);
  }
}
