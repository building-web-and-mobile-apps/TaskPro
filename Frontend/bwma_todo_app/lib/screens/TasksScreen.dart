import 'package:bwma_todo_app/model/Category.dart';
import 'package:bwma_todo_app/model/Priority.dart';
import 'package:bwma_todo_app/model/UserDTO.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../model/Task.dart';
import 'AddTaskScreen.dart';
import 'AdminScreen.dart';
import 'TaskDetailScreen.dart';
import '../../core/TaskFutures.dart';
import 'TasksFilterScreen.dart';
import 'WeatherScreen.dart';
import 'chat/ChatsScreen.dart';
import 'package:csv/csv.dart';
import 'package:universal_html/html.dart' as html;
import 'package:intl/intl.dart';

class TasksScreen extends StatefulWidget {
  final TaskFutures taskFutures;
  final String token;

  TasksScreen({Key? key, required this.taskFutures, required this.token})
      : super(key: key);

  @override
  _TasksScreenState createState() => _TasksScreenState();
}

class _TasksScreenState extends State<TasksScreen> {
  late List<TaskModel> tasks;

  late String sortBy;
  late String direction;

  @override
  void initState() {
    super.initState();
    tasks = [];
    _fetchTasks();
    sortBy = 'title';
    direction = 'asc';
  }

  Future<void> _fetchTasks() async {
    try {
      final fetchedTasks = await widget.taskFutures.fetchFilteredTasks();
      setState(() {
        tasks = fetchedTasks;
      });
    } catch (e) {
      print(e);
    }
  }

  void _openFilePicker() async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles();

      if (result != null) {
        Uint8List fileBytes = result.files.first.bytes!;
        String content = String.fromCharCodes(fileBytes);
        bool successful = await widget.taskFutures.addBulkTasks(content);
        await _fetchTasks();
        if (successful) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Tasks imported successfully'),
              duration: Duration(seconds: 2),
            ),
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Error importing tasks'),
              duration: Duration(seconds: 2),
            ),
          );
        }
      } else {
        print('No file selected');
      }
    } catch (e) {
      print('Error picking file: $e');
    }
  }

  void _exportTasks(BuildContext context, List<TaskModel> tasks) {
    try {
      List<List<dynamic>> csvData = [
        [
          'Title',
          'Description',
          'Done',
          'DueDate',
          'CompletedDate',
          'Archived',
          'UserId',
          'Priority',
          'Category'
        ],
        for (var task in tasks)
          [
            task.name,
            task.description,
            task.done,
            DateFormat("yyyy-MM-ddTHH:mm:ss").format(task.dueDate),
            task.completedDate != null
                ? DateFormat("yyyy-MM-ddTHH:mm:ss").format(task.dueDate)
                : '',
            task.archived,
            task.user.userId,
            task.priority.name,
            task.category.name
          ]
      ];

      String csvString =
          const ListToCsvConverter(fieldDelimiter: ';').convert(csvData);

      final blob = html.Blob([csvString], 'text/csv');

      final url = html.Url.createObjectUrlFromBlob(blob);

      html.AnchorElement(href: url)
        ..setAttribute('download', 'tasks_export.csv')
        ..click();

      html.Url.revokeObjectUrl(url);

      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Tasks exported successfully'),
          duration: Duration(seconds: 2),
        ),
      );
    } catch (e) {
      // Handle errors
      print('Error exporting tasks: $e');
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Error exporting tasks: $e'),
          duration: const Duration(seconds: 2),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.alphaBlend(
            fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
        title: Text(
          'Tasks',
          style: TextStyle(
            color: fromInt(0xFFE4E1E6),
          ),
        ),
        actions: [
          FutureBuilder<String?>(
            future: widget.taskFutures.getUserRoleByUserId(FirebaseAuth.instance.currentUser!.uid),
            builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
              if (snapshot.hasData && snapshot.data == 'admin') {
                return IconButton(
                  icon: Icon(Icons.admin_panel_settings),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AdminScreen(taskFutures: widget.taskFutures)),
                    );
                  },
                );
              } else {
                return Container(); // return an empty container when user is not an admin
              }
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.search,
              color: Color(0xFFE4E1E6),
            ),
            onPressed: () {
              _showSearchDialog();
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.refresh,
              color: Color(0xFFE4E1E6),
            ),
            onPressed: () async {
              await _fetchTasks();
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Tasks refreshed!'),
                  duration: Duration(seconds: 2),
                ),
              );
            },
          ),
          _buildSortDropdown(),
          IconButton(
            icon: const Icon(
              Icons.filter_list,
              color: Color(0xFFE4E1E6),
            ),
            onPressed: () {
              _openFilterScreen();
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.exit_to_app,
              color: Color(0xFFE4E1E6),
            ),
            onPressed: () {
              _signOut();
            },
          ),

        ],
        iconTheme: IconThemeData(
          color: fromInt(0xFFE4E1E6),
        ),
      ),
      drawer: Drawer(
        child: Container(
          color: Color.alphaBlend(
              fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
          child: ListView(
            children: [
              ListTile(
                title:
                    const Text('Tasks', style: TextStyle(color: Colors.white)),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Weather',
                    style: TextStyle(color: Colors.white)),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            WeatherScreen(token: widget.token)),
                  );
                },
              ),
              ListTile(
                title:
                    const Text('Chat', style: TextStyle(color: Colors.white)),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ChatsScreen()),
                  );
                },
              ),
              ListTile(
                title:
                    const Text('Import', style: TextStyle(color: Colors.white)),
                onTap: () {
                  if (!kIsWeb) {
                    Navigator.pop(context);
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('File import not supported on mobile!'),
                        duration: Duration(seconds: 2),
                      ),
                    );
                    return;
                  }
                  _openFilePicker();
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title:
                    const Text('Export', style: TextStyle(color: Colors.white)),
                onTap: () {
                  if (!kIsWeb) {
                    Navigator.pop(context);
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('File export not supported on mobile!'),
                        duration: Duration(seconds: 2),
                      ),
                    );
                    return;
                  }
                  _exportTasks(context, tasks);
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ),
      backgroundColor: Color.alphaBlend(
          fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final result = await Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) =>
                  AddTaskScreen(taskFutures: widget.taskFutures),
            ),
          );
          if (result == true) {
            _fetchTasks();
          }
        },
        backgroundColor: const Color(0xFF0039B4),
        child: const Icon(
          Icons.add,
          color: Color(0xFFE4E1E6),
        ),
      ),
      body: ListView.builder(
        itemCount: tasks.length,
        itemBuilder: (context, index) {
          final entry = tasks[index];
          return _buildSingleTask(index, tasks, entry, context);
        },
      ),
    );
  }

  Widget _buildSingleTask(
      int index, List<TaskModel> taskList, TaskModel entry, BuildContext context) {
    return Card(
      color: Color.alphaBlend(
          fromInt(0xFFB7C4FF).withOpacity(0.14), fromInt(0xFF1B1B1F)),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: ListTile(
          contentPadding: const EdgeInsets.symmetric(vertical: 8.0),
          title: Text(
            entry.name,
            style: entry.done
                ? const TextStyle(
                    decoration: TextDecoration.lineThrough,
                    decorationThickness: 3,
                    fontSize: 20,
                    color: Color(0xFFE4E1E6),
                  )
                : const TextStyle(
                    fontSize: 20,
                    color: Color(0xFFE4E1E6),
                  ),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 8),
              Text(
                entry.description,
                style: const TextStyle(
                  fontSize: 16,
                  color: Color(0xFFE4E1E6),
                ),
              ),
            ],
          ),
          trailing: SizedBox(
            width: 60,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Transform.scale(
                  scale: 1.3,
                  child: Checkbox(
                    side:
                        const BorderSide(color: Color(0xFFc2c5dd), width: 1.5),
                    shape: const CircleBorder(),
                    checkColor: Colors.white,
                    activeColor: const Color(0xFF094EE7),
                    value: entry.done,
                    onChanged: (bool? value) {
                      CreatePriority createPriority = CreatePriority(
                        id: entry.priority.id,
                      );

                      CreateCategory createCategory = CreateCategory(
                        id: entry.category.id,
                      );

                      CreateUser createUser = CreateUser(
                        userId: entry.user.userId,
                      );

                      CreateTask statusTask = CreateTask(
                        name: entry.name,
                        description: entry.description,
                        dueDate: entry.dueDate,
                        priority: createPriority,
                        category: createCategory,
                        done: !entry.done,
                        taskId: entry.taskId,
                        user: createUser,
                        archived: entry.archived,
                      );
                      Future<bool> response =
                          widget.taskFutures.updateStatus(statusTask);
                      response.then((value) {
                        if (value) {
                          setState(() {
                            entry.done = !entry.done;
                          });
                        }
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
          onTap: () async {
            final result = await Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => TaskDetailScreen(
                        taskFutures: widget.taskFutures,
                        task: entry,
                        taskId: entry.taskId,
                      )),
            );
            if (result == true) {
              _fetchTasks();
            }
          },
        ),
      ),
    );
  }

  void _showSearchDialog() async {
    String? keyword;
    keyword = await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Search Tasks'),
          content: TextField(
            decoration: const InputDecoration(labelText: 'Enter keyword'),
            onChanged: (value) {
              keyword = value; // Capture the entered value
            },
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () async {
                if (keyword != null && keyword!.isNotEmpty) {
                  var searchedTasks =
                      await widget.taskFutures.searchTasks(keyword!);
                  setState(() {
                    tasks = searchedTasks;
                  });
                }
                Navigator.of(context).pop();
              },
              child: const Text('Search'),
            ),
          ],
        );
      },
    );
  }

  Widget _buildSortDropdown() {
    return PopupMenuButton<String>(
      color: const Color(0xFFE4E1E6),
      onSelected: (value) async {
        setState(() {
          sortBy = value;
          direction = direction == 'asc' ? 'desc' : 'asc'; // Toggle direction
        });
        await _fetchSortedTasks();
      },
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          value: 'title',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Title'),
              if (sortBy == 'title') _buildArrowIcon(),
            ],
          ),
        ),
        PopupMenuItem(
          value: 'dueDate',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Due Date'),
              if (sortBy == 'dueDate') _buildArrowIcon(),
            ],
          ),
        ),
        PopupMenuItem(
          value: 'done',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Status'),
              if (sortBy == 'done') _buildArrowIcon(),
            ],
          ),
        ),
        PopupMenuItem(
          value: 'description',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Description'),
              if (sortBy == 'description') _buildArrowIcon(),
            ],
          ),
        ),
        PopupMenuItem(
          value: 'priority',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Priority'),
              if (sortBy == 'priority') _buildArrowIcon(),
            ],
          ),
        ),
        PopupMenuItem(
          value: 'category',
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text('Category'),
              if (sortBy == 'category') _buildArrowIcon(),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildArrowIcon() {
    return Icon(
      direction == 'asc' ? Icons.arrow_drop_up : Icons.arrow_drop_down,
    );
  }

  Future<void> _fetchSortedTasks() async {
    try {
      // final taskFutures = TaskFutures();
      final sortedTasks = await widget.taskFutures.sortTasks(sortBy, direction);
      setState(() {
        tasks = sortedTasks;
      });
    } catch (e) {
      print(e);
    }
  }

  void _openFilterScreen() async {
    final filteredTasks = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => TaskFilterScreen(taskFutures: widget.taskFutures),
      ),
    );

    if (filteredTasks != null) {
      setState(() {
        tasks = filteredTasks;
      });
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Filters applied!'),
          duration: Duration(seconds: 2),
        ),
      );
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('No filters applied!'),
          duration: Duration(seconds: 2),
        ),
      );
    }
  }

  static Color fromInt(int hexInt) {
    return Color(hexInt);
  }

  Future<void> _signOut() async {
    try {
      await FirebaseAuth.instance.signOut();
      // Perform any additional actions after signing out, if needed
      // For example, navigate to a login screen

      try {
        User? user = FirebaseAuth.instance.currentUser;

        if (user != null) {
          FirebaseFirestore firestore = FirebaseFirestore.instance;

          await firestore.collection('users').doc(user.uid).update({
            'lastActive': Timestamp.now(),
            'isOnline': false,
          });

          // Sign out
          await FirebaseAuth.instance.signOut();

          // Navigate to login screen
          Navigator.of(context).pushReplacementNamed('/sign-in');
        } else {
          print('No user signed in');
        }
      } catch (e) {
        print("Error during sign out: $e");
      }

      Navigator.of(context).pushReplacementNamed(
          '/sign-in'); // Replace with your login screen route
    } catch (e) {
      print("Error during sign out: $e");
    }
  }
}
