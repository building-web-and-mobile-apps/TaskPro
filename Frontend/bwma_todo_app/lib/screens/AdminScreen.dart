import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:bwma_todo_app/core/TaskFutures.dart';

class AdminScreen extends StatefulWidget {
  final TaskFutures taskFutures;

  const AdminScreen({Key? key, required this.taskFutures}) : super(key: key);

  @override
  _AdminScreenState createState() => _AdminScreenState();
}

class _AdminScreenState extends State<AdminScreen> {
  List<Map<String, dynamic>> users = [];

  Future<void> getAllUsers() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    QuerySnapshot querySnapshot = await firestore.collection('users').get();
    users = querySnapshot.docs.map((doc) => doc.data() as Map<String, dynamic>).toList();
  }

  Future<void> _removeUser(String userId, int index) async {
    final confirm = await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Confirm'),
        content: Text('Are you sure you want to remove this user?'),
        actions: [
          TextButton(
            child: Text('Cancel'),
            onPressed: () => Navigator.of(context).pop(false),
          ),
          TextButton(
            child: Text('OK'),
            onPressed: () => Navigator.of(context).pop(true),
          ),
        ],
      ),
    );

    if (confirm) {

      setState(() {
        users.removeAt(index);
      });
      await widget.taskFutures.removeAccount(userId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Admin Panel'),
      ),
      body: FutureBuilder(
        future: getAllUsers(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else {
            return ListView.builder(
              itemCount: users.length,
              itemBuilder: (context, index) {
                Map<String, dynamic> user = users[index];
                return ListTile(
                  title: Text(user['name'] ?? 'No name'),
                  subtitle: Text(user['email'] ?? 'No email'),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      AdminToggleButton(
                        uid: user['uid'],
                        isAdmin: user['admin'] ?? false,
                        taskFutures: widget.taskFutures,
                      ),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          _removeUser(user['uid'] ?? '', index);
                        },
                      ),
                    ],
                  ),
                );
              },
            );
          }
        },
      ),
    );
  }
}

class AdminToggleButton extends StatefulWidget {
  final String? uid;
  bool isAdmin;
  final TaskFutures taskFutures;

  AdminToggleButton({this.uid, required this.isAdmin, required this.taskFutures});

  @override
  _AdminToggleButtonState createState() => _AdminToggleButtonState();
}

class _AdminToggleButtonState extends State<AdminToggleButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(widget.isAdmin ? Icons.remove_circle : Icons.add_circle),
      onPressed: () {
        setState(() {
          widget.isAdmin = !widget.isAdmin;
        });
        _toggleAdminRights(widget.uid ?? '');
      },
    );
  }

  Future<void> _toggleAdminRights(String userId) async {
    String? userRole = await widget.taskFutures.getUserRoleByUserId(userId);
    bool isAdmin = userRole == 'admin';
    if (isAdmin) {
      await widget.taskFutures.removeAdmin(userId);
    } else {
      await widget.taskFutures.addAdmin(userId);
    }
  }
}