import 'package:bwma_todo_app/core/TaskFutures.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../model/Attachment.dart';
import '../model/Task.dart';
import 'EditTasksScreen.dart';

class TaskDetailScreen extends StatefulWidget {
  final TaskModel task;
  final int taskId;
  final TaskFutures taskFutures;

  TaskDetailScreen({
    super.key,
    required this.task,
    required this.taskId,
    required this.taskFutures,
  });

  @override
  _TaskDetailScreenState createState() => _TaskDetailScreenState();
}

class _TaskDetailScreenState extends State<TaskDetailScreen> {
  late TaskModel? _task;
  List<Attachment> _attachments = [];

  @override
  void initState() {
    super.initState();
    _task = widget.task;
    _fetchAttachmentsByTaskId();
  }

  Future<List<Attachment>> _fetchAttachmentsByTaskId() async {
    try {
      final attachments =
          await widget.taskFutures.fetchAttachmentsByTaskId(widget.taskId);
      setState(() {
        _attachments = attachments;
      });
      return attachments;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Widget _buildAttachmentList() {
    if (_attachments.isEmpty) {
      return const Text(
        'No attachments found',
        style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
      );
    }

    return ListView.builder(
      shrinkWrap: true,
      itemCount: _attachments.length,
      itemBuilder: (context, index) {
        final attachment = _attachments[index];
        return ListTile(
          title: Text(
            attachment.fileName,
            style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
          ),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(
                icon: const Icon(Icons.download, color: Color(0xFFE4E1E6),),
                onPressed: () {
                  if (!kIsWeb) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('File attachments are not supported on mobile!'),
                        duration: Duration(seconds: 2),
                      ),
                    );
                    return;
                  }
                  widget.taskFutures.downloadAttachment(attachment.filePath);
                },
              ),
              IconButton(
                icon: const Icon(Icons.delete, color: Color(0xFFE4E1E6),),
                onPressed: () async {
                  if(!kIsWeb) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('File attachments are not supported on mobile!'),
                        duration: Duration(seconds: 2),
                      ),
                    );
                    return;
                  }
                  bool deletedSuccess = await widget.taskFutures
                      .deleteAttachmentByTask(widget.taskId, attachment.id);
                  if (deletedSuccess) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Attachment deleted successfully'),
                        duration: Duration(seconds: 2),
                      ),
                    );
                    List<Attachment> attachments = await widget.taskFutures
                        .fetchAttachmentsByTaskId(widget.taskId);
                    setState(() {
                      _attachments = attachments;
                    });
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Error deleting attachment'),
                        duration: Duration(seconds: 2),
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        );
      },
    );
  }

  Future<TaskModel?> _fetchTaskById(int taskId) async {
    try {
      final fetchedTask = await widget.taskFutures.fetchTaskById(taskId);
      setState(() {
        _task = fetchedTask;
      });
      return fetchedTask;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void _openFilePicker() async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles();

      if (result != null) {
        String fileName = result.files.first.name;
        Uint8List fileBytes = result.files.first.bytes!;
        bool successful = await widget.taskFutures
            .uploadFileToTask(fileBytes, widget.taskId, fileName);
        await _fetchTaskById(widget.taskId);
        await _fetchAttachmentsByTaskId();
        if (successful) {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('File attachment uploaded successfully'),
              duration: Duration(seconds: 2),
            ),
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(
              content: Text('Error uploading file attachment'),
              duration: Duration(seconds: 2),
            ),
          );
        }
      } else {
        print('No file selected');
      }
    } catch (e) {
      print('Error picking file: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.alphaBlend(
        fromInt(0xFFB7C4FF).withOpacity(0.14),
        fromInt(0xFF1B1B1F),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showDeleteConfirmationDialog(context);
        },
        backgroundColor: Colors.red,
        tooltip: 'Delete Task',
        child: const Icon(
          Icons.delete,
          color: Colors.white,
        ),
      ),
      appBar: AppBar(
        backgroundColor: Color.alphaBlend(
          fromInt(0xFFB7C4FF).withOpacity(0.08),
          fromInt(0xFF1B1B1F),
        ),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.upload_file,
              color: Color(0xFFE4E1E6),
            ),
            onPressed: () {
              if (!kIsWeb) {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('File attachments are not supported on mobile!'),
                    duration: Duration(seconds: 2),
                  ),
                );
                return;
              }
              _openFilePicker();
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.edit,
              color: Color(0xFFE4E1E6),
            ),
            onPressed: () async {
              final result = await Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => EditTasksScreen(
                    taskFutures: widget.taskFutures,
                    task: _task,
                    taskId: widget.taskId,
                  ),
                ),
              );
              if (result == true) {
                _fetchTaskById(widget.taskId);
              }
            },
          ),
        ],
        title: const Text(
          'Task Detail',
          style: TextStyle(color: Color(0xFFE4E1E6)),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Color(0xFFE4E1E6),
          ),
          onPressed: () async {
            Navigator.of(context).pop(true);
          },
        ),
      ),
      body: FutureBuilder<TaskModel?>(
        future: widget.taskFutures.fetchTaskById(widget.taskId),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return Text(
              "Error: ${snapshot.error}",
              style: const TextStyle(
                color: Color(0xFFE4E1E6),
              ),
            );
          } else if (!snapshot.hasData || snapshot.data == null) {
            return const Center(
              child: Text(
                'No task found',
                style: TextStyle(
                  color: Color(0xFFE4E1E6),
                ),
              ),
            );
          } else {
            return _buildTaskDetails(snapshot.data!);
          }
        },
      ),
    );
  }

  Widget _buildTaskDetails(TaskModel task) {
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Name:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              Text(
                task.name,
                style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
              ),
              const SizedBox(height: 20),
              const Text(
                'Description:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              Text(
                task.description,
                style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
              ),
              const SizedBox(height: 20),
              const Text(
                'Status:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              Text(
                task.done ? 'Completed' : 'Not Completed',
                style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
              ),
              const SizedBox(height: 20),
              const Text(
                'Due Date:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              Text(
                task.dueDate.toString(), // Replace with formatted due date
                style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
              ),
              const SizedBox(height: 20),
              const Text(
                'Priority:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              Text(
                task.priority.name,
                style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
              ),
              const SizedBox(height: 20),
              const Text(
                'Category:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              Text(
                task.category.name,
                style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
              ),
              const SizedBox(height: 20),
              const Text(
                'Completed Date:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              Text(
                task.completedDate == null
                    ? 'Not Completed'
                    : task.completedDate.toString(),
                style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
              ),
              const SizedBox(height: 20),
              const Text(
                'Archived:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              Text(
                task.archived ? 'Yes' : 'No',
                style: const TextStyle(color: Color(0xFFE4E1E6), fontSize: 16),
              ),
              const SizedBox(height: 20),
              const Text(
                'Attachments:',
                style: TextStyle(color: Color(0xFFE4E1E6), fontSize: 18),
              ),
              const SizedBox(height: 8),
              _buildAttachmentList(),
              const SizedBox(height: 75),
            ],
          ),
        ),
      ),
    );
  }

  void _showDeleteConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Delete Task'),
          content: const Text('Are you sure you want to delete this task?'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            ElevatedButton(
              onPressed: () async {
                await widget.taskFutures.deleteTask(widget.taskId);
                Navigator.of(context).pop();
                Navigator.of(context).pop(true);
              },
              child: const Text('Delete'),
            ),
          ],
        );
      },
    );
  }

  static Color fromInt(int hexInt) {
    return Color(hexInt);
  }
}
