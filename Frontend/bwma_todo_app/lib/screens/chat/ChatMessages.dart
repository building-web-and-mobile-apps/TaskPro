import 'package:bwma_todo_app/model/Message.dart';
import 'package:bwma_todo_app/screens/chat/widgets/EmptyWidget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../core/provider/FirebaseProvider.dart';
import 'MessageBubble.dart';

class ChatMessages extends StatelessWidget {
  const ChatMessages({super.key, required this.receiverId});

  final String receiverId;

  @override
  Widget build(BuildContext context) {
    return Consumer<FirebaseProvider>(
      builder: (context, value, child) => value.messages.isEmpty
          ? const EmptyWidget(icon: Icons.waving_hand, text: 'Say Hello!')
          : Expanded(
              child: ListView.builder(
                controller: Provider.of<FirebaseProvider>(context, listen: false).scrollController,
                itemCount: value.messages.length,
                itemBuilder: (context, index) {
                  final isTextMessage = value.messages[index].messageType == MessageType.text;
                  final isMe = value.messages[index].senderId == value.user!.uid;

                  return isTextMessage
                      ? MessageBubble(
                          message: value.messages[index],
                          isMe: isMe,
                          isImage: false,
                        )
                      : MessageBubble(
                          message: value.messages[index],
                          isMe: isMe,
                          isImage: true,
                        );
                },
              ),
            ),
    );
  }
}
