import 'package:bwma_todo_app/model/UserChat.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'ChatScreen.dart';

class UserItem extends StatefulWidget {
  const UserItem({super.key, required this.user});

  final UserModel user;

  @override
  State<UserItem> createState() => _UserItemState();
}

class _UserItemState extends State<UserItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => ChatScreen(userId: widget.user.uid),
        ));
      },
      child: ListTile(
          contentPadding: EdgeInsets.zero,
          leading: const Stack(alignment: Alignment.bottomRight, children: [
            CircleAvatar(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/images/User.jpeg'),
              ),
            ),
          ]),
          title: Text(
            widget.user.name,
            style: const TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          subtitle: Text(
            'Last active: ${timeago.format(widget.user.lastActive)}',
            maxLines: 2,
            style: const TextStyle(
              color: Colors.grey,
              fontSize: 15,
              overflow: TextOverflow.ellipsis,
            ),
          )),
    );
  }
}
