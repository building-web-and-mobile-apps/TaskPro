import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../core/provider/FirebaseProvider.dart';
import 'ChatMessages.dart';
import 'ChatTextField.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({super.key, required this.userId});

  final String userId;

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  void initState() {
    Provider.of<FirebaseProvider>(context, listen: false)
        ..getUserById(widget.userId)
        ..getMessages(widget.userId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(children: [
            Expanded(
              child: ChatMessages(
                receiverId: widget.userId,
              ),
            ),
            ChatTextField(
              receiverId: widget.userId,
            ),
          ])),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      foregroundColor: Colors.black,
      title: Consumer<FirebaseProvider>(
        builder: (context, value, child) => value.user != null
            ? Row(
                children: [
                  const CircleAvatar(
                    backgroundImage: AssetImage('assets/images/User.jpeg'),
                  ),
                  const SizedBox(width: 10),
                  Column(
                    children: [
                      Text(
                        value.user!.name,
                        style: const TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ],
              )
            : const SizedBox(),
      ),
    );
  }
}
