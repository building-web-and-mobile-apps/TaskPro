import 'package:bwma_todo_app/model/UserDTO.dart';

import 'Category.dart';
import 'Priority.dart';

class TaskModel {
  int taskId;
  String name;
  String description;
  bool done;
  DateTime dueDate;
  DateTime? completedDate;
  bool archived;
  UserDTO user;
  Category category;
  Priority priority;

  TaskModel({
    required this.taskId,
    required this.name,
    required this.description,
    required this.done,
    required this.dueDate,
    this.completedDate,
    required this.archived,
    required this.user,
    required this.category,
    required this.priority,
  });

  factory TaskModel.fromJson(Map<String, dynamic> json) {
    return TaskModel(
      taskId: json['id'],
      name: json['title'],
      description: json['description'],
      done: json['done'],
      dueDate: DateTime.parse(json['dueDate']),
      completedDate: json['completeDate'] != null
          ? DateTime.parse(json['completeDate'])
          : null,
      archived: json['archived'],
      user: UserDTO.fromJson(json['user']),
      category: Category.fromJson(json['category']),
      priority: Priority.fromJson(json['priority']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': name,
      'description': description,
      'done': done,
      'dueDate': dueDate.toIso8601String(),
      'priority': priority.toJson(),
      'category': category.toJson(),
      'completeDate': completedDate?.toIso8601String(),
      'archived': archived,
      'user': user.toJson(),
    };
  }
}

class CreateTask {
  int taskId;
  String name;
  String description;
  bool done;
  DateTime dueDate;
  DateTime? completedDate;
  bool archived;
  CreateUser user;
  CreateCategory category;
  CreatePriority priority;

  CreateTask({
    required this.taskId,
    required this.name,
    required this.description,
    required this.done,
    required this.dueDate,
    this.completedDate,
    required this.archived,
    required this.user,
    required this.category,
    required this.priority,
  });

  Map<String, dynamic> toJson() {
    return {
      'title': name,
      'description': description,
      'done': done,
      'dueDate': dueDate.toIso8601String(),
      'priority': priority.toJson(),
      'category': category.toJson(),
      'completeDate': completedDate?.toIso8601String(),
      'archived': archived,
      'user': user.toJson(),
    };
  }

  factory CreateTask.fromJson(Map<String, dynamic> json) {
    return CreateTask(
      taskId: json['id'],
      name: json['title'],
      description: json['description'],
      done: json['done'],
      dueDate: DateTime.parse(json['dueDate']),
      completedDate: json['completeDate'] != null
          ? DateTime.parse(json['completeDate'])
          : null,
      archived: json['archived'],
      user: CreateUser.fromJson(json['user']),
      category: CreateCategory.fromJson(json['category']),
      priority: CreatePriority.fromJson(json['priority']),
    );
  }
}