class Message {
  final String receiverId;
  final String senderId;
  final String content;
  final DateTime sentTime;
  final MessageType messageType;

  const Message(
      {required this.receiverId,
      required this.senderId,
      required this.content,
      required this.sentTime,
      required this.messageType});

  Map<String, dynamic> toJson() => {
        'receiverId': receiverId,
        'senderId': senderId,
        'content': content,
        'sentTime': sentTime,
        'messageType': messageType.toJson(),
      };

  factory Message.fromJson(Map<String, dynamic> json) =>
      Message(
        receiverId: json['receiverId'],
        senderId: json['senderId'],
        sentTime: json['sentTime'].toDate(),
        content: json['content'],
        messageType:
        MessageType.fromJson(json['messageType']),
      );
}

enum MessageType {
  text;

  String toJson() => name;

  factory MessageType.fromJson(String json) =>
      values.byName(json);
}
