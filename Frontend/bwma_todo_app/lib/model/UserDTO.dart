class UserDTO {
  final int id;
  final String email;
  final String name;
  final String userId;
  final DateTime createdAt;
  final DateTime updatedAt;
  final String role;


  UserDTO({
    required this.id,
    required this.email,
    required this.name,
    required this.userId,
    required this.createdAt,
    required this.updatedAt,
    required this.role
  });

  factory UserDTO.fromJson(Map<String, dynamic> json) {
    return UserDTO(
      id: json['id'],
      email: json['email'],
      name: json['name'],
      userId: json['userId'] ?? '',
      createdAt: DateTime.parse(json['createdAt']),
      updatedAt: DateTime.parse(json['updatedAt']),
      role: json['role'] ?? 'user',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'email': email,
      'name': name,
      'userId': userId,
      'createdAt': createdAt.toIso8601String(),
      'updatedAt': updatedAt.toIso8601String(),
      'role': role,
    };
  }
}

class CreateUser{
  final String userId;

  CreateUser({
    required this.userId,
  });

  Map<String, dynamic> toJson() {
    return {
      'userId': userId,
    };
  }

  factory CreateUser.fromJson(Map<String, dynamic> json) {
    return CreateUser(
      userId: json['userId'],
    );
  }
}
