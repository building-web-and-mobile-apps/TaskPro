class Attachment {
  final int id;
  final String fileName;
  final String fileType;
  final int size;
  final String filePath;

  Attachment({
    required this.id,
    required this.fileName,
    required this.fileType,
    required this.size,
    required this.filePath,
  });

  factory Attachment.fromJson(Map<String, dynamic> json) {
    return Attachment(
      id: json['id'] as int,
      fileName: json['fileName'] as String,
      fileType: json['fileType'] as String,
      size: json['size'] as int,
      filePath: json['filePath'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'fileName': fileName,
      'fileType': fileType,
      'size': size,
      'filePath': filePath,
    };
  }
}
