class Priority {
  final int id;
  final String name;

  Priority({required this.name, required this.id});

  factory Priority.fromJson(Map<String, dynamic> json) {
    return Priority(
      name: json['name'],
      id: json['id'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'id': id,
    };
  }
}

class CreatePriority{
  final int id;

  CreatePriority({
    required this.id,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }

  factory CreatePriority.fromJson(Map<String, dynamic> json) {
    return CreatePriority(
      id: json['id'],
    );
  }
}