class Category {
  final int id;
  final String name;

  Category({required this.name, required this.id});

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      name: json['name'],
      id: json['id'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'id': id,
    };
  }
}

class CreateCategory {
  final int id;

  CreateCategory({
    required this.id,
  });

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }

  factory CreateCategory.fromJson(Map<String, dynamic> json) {
    return CreateCategory(
      id: json['id'],
    );
  }
}