# Frontend

Welcome to the TaskPro Frontend repository! This repository contains the source code for the frontend built in Flutter. This frontend provides the user interface for our TaskPro application. Besides a web application we also offer a running android app.

## Installation and Setup
To run the frontend, follow these steps:

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.cs.hs-fulda.de/fdai6557/bwma-project-todos.git
   cd bwma-project-todos/Frontend/bwma_todo_app/

2. **Used Technologies:**
- [Flutter](https://flutter.dev/)
- [Android Studio](https://developer.android.com/studio)

3. **Install Dependencies**
    ```bash 
    flutter pub get

4. **Run the Application**
    ```bash
    flutter run -d <Your Device>

    # For example
    flutter run -d Chrome

This will launch our TaskPro application on your local machine. Make sure you have Flutter installed and properly set up on your development environment. Also make sure to have either your phone, browser or any other device ready for starting the application.

Another and even better approach would be to install Android Studio:

- **Install Android Studio**
Follow the official instructions to download and install Android Studio from [here](https://developer.android.com/studio).

- **Install Flutter Plugin**
Before you can start the application you have to install the Flutter and Dart Plugin in Android Studio.

- **Connect a Device**
Connect to your Chrome browser, your Android device or use the built in emulator. Make sure USB debugging is enabled if you use your own Android device.

 - **Run the Application from Android Studio**
Open the Frontend folder of our project in Android Studio. Choose your connected device or emulator. Click on the "Run" button and our Todo Application will be launched.

## Example Task List Screen

<img src="Frontend/bwma_todo_app/assets/images/TaskListScreen_Screenshot.png" alt="TaskListScreen Screenshot" width="400"/>

# Mobile Application

Our mobile application is also developed using Flutter. It offers almost the same functionality as the web version.

## Installation and Setup
The Installation and Setup for the Mobile Application is the same as the "normal" Installation and Setup. Since we are using Flutter we support all plattforms with a single code base.

# Main Application Flow

Our application will always start with the login/registration screen unless the user is logged in. On this page the user can login with email and password or with Google. They can also register or run a forgotten password flow. After logging in, the user is taken to the main screen, which displays all tasks for the logged in user. From this screen, the user can click on a task to view details, add new tasks, sort tasks, filter tasks, search tasks, refresh tasks and sign out. It is also possible to update the details of a task and delete tasks from the details screen for a single task. The details screen also allows the user to manage file attachments. Returning to the main screen. Here we also have a drawer element in the top left corner. On this menu we can see more features such as our Weather API integration, a chat feature for all users of the application to chat and the import and export features.

# Features

- Management of Tasks (CRUD Operations)
- Import + Export of Tasks
- Weather API Integration
- Searching, Filter & Sorting
- Login + Registration
- Google Login/Registration
- Chat Feature
- File upload as Attachements to Tasks
- Multifactor Authentication

## Import and Export of Tasks

The user can import and export tasks in the form of CSV files. For example, the user can import tasks from a CSV file that was exported from another TaskPro application. The user can also export tasks to a CSV file and share it with other users.
For importing the CSV file must follow the following structure (plus example):

    Title;Description;Done;DueDate;CompletedDate;Archived;Priority;Category
    Task1;Description1;false;2024-02-26T07:00:00;2024-02-26T07:00:00;false;High;Work

## Side note

Some features like import and export & file upload as attachements to tasks is only available in the web version of our application. The mobile application does not support these features because of the limitations of the mobile devices and differences in file systems.